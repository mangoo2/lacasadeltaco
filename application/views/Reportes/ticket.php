<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    $cambio=$vcambio;
    $pagacon=$vpagacon;
    $metodo=$metodo;
    foreach ($configticket->result() as $item){
      $GLOBALS['titulo'] = $item->titulo;
      $GLOBALS['mensaje'] = $item->mensaje;
      $GLOBALS['mensaje2'] = $item->mensaje2;
      $fuente = $item->fuente;
      $GLOBALS['tamano'] = $item->tamano;
      $GLOBALS['margensup'] = $item->margensup;
    }
    foreach ($getventas->result() as $item){
      $GLOBALS['idticket']= $item->id_venta;
      $id_personal = $item->id_personal;
      $id_cliente = $item->id_cliente;
      $metodo = $item->metodo;
      $pagot = $item->pagotarjeta;
      $pagoe= $item->efectivo;
      $tipo= $item->tipo_costo;
      $direcc= $item->direcc;
      //log_message('error', 'metodo: '.$metodo);
      if($metodo==1){
        $metodo="Efectivo";
      }
      else if($metodo==2){
        $metodo="Tarjeta de crédito";
      }
      else if($metodo==3){
        $metodo="Tarjeta de débito";
      }
      else if($metodo==4){
        $metodo="Plataforma";
        $pagacon=0;
      }

      if($tipo==1)
        $tipotxt="Mostrador";
      else if($tipo==2)
        $tipotxt="Mesa";
      else if($tipo==3)
        $tipotxt="A Domicilio";

      $GLOBALS['subtotal'] = $item->subtotal;
      $GLOBALS['descuento'] = $item->descuento;
      $GLOBALS['monto_total'] = $item->monto_total;
      $reg = $item->reg;
    }
    $GLOBALS['fecha']= date("d-m-Y",strtotime($reg));
    $GLOBALS['hora']= date("G:i",strtotime($reg));
    if ($fuente==1) {
      $GLOBALS['tipofuente'] = "arial";
    }
    if ($fuente==2) {
      $GLOBALS['tipofuente'] = "Times New Roman";
    }
    if ($fuente==3) {
      $GLOBALS['tipofuente'] = "Open Sans";
    }
    if ($fuente==4) {
      $GLOBALS['tipofuente'] = "Calibri";
    }
    //log_message('error', 'pagacon: '.$pagacon);
    //log_message('error', 'pagot: '.$pagot);
    //log_message('error', 'pagoe: '.$pagoe);
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $img_header = base_url() . 'public/img/LaCasaDelTaco_H.png';
      //$this->Image($img_header, 0, 0, 130, 130, '', '', 'C', true, 100, '', false, false, 0);
      $this->Image($img_header, 10, 5, 40, 0, '', '', 'C', true, 100, '', false, false, 0, false);
      //$imglogo = base_url() . 'public/img/mangoo_h.png';
      $imglogo = '';

      $html = '
          <table width="100%" border="0" >
            <tr>
              <td colspan="3" align="center" height="170"><img src="'.$imglogo.'" width="130px" ></td>
            </tr>
            <tr>
              <th colspan="3" align="center" style=" font-size: '.$GLOBALS['tamano'].';text-align: left; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['titulo'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" ></th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['mensaje'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">Fecha: '.$GLOBALS['fecha'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: '.$GLOBALS['tamano'].'; ;font-family: '.$GLOBALS['tipofuente'].';">Hora: '.$GLOBALS['hora'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: '.$GLOBALS['tamano'].'; ;font-family: '.$GLOBALS['tipofuente'].';">No cuenta: '.$GLOBALS['idticket'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9"></th>
            </tr>
          </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(60, 210), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '76', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();

$txttable="";
if($tipo==3){
  $txttable="Domicilio: ".$direcc;
}

$html='<table border="0" style="margin-top:0;margin-bottom:0;">
          <tr>
            <th colspan="3" align="left" style="font-size: '.$GLOBALS['tamano'].'; ;font-family: '.$GLOBALS['tipofuente'].';">'.$txttable.'
            </th>
          </tr>
          <tr>
            <th colspan="3" align="center" style="background-color: black; color:white">Cuenta
            </th>
          </tr>
          <tr>
            <th width="20%" style="font-size: 8; font-family: '.$GLOBALS['tipofuente'].';" align="center">Cant.</th>
            <th width="50%" style="font-size: 8; font-family: '.$GLOBALS['tipofuente'].';" align="center">Producto</th>
            <th width="30%" style="font-size: 8; font-family: '.$GLOBALS['tipofuente'].';" align="center">Precio</th>
          </tr>';
foreach ($getventasd->result() as $rowEmp){
    $html .= '
            <tr>
                <th style="font-size: 7" align="center">'.$rowEmp->cantidad.'</th>
                <th style="font-size: 7" align="center">'.$rowEmp->nombre.'</th>
                <th style="font-size: 7" align="center">$'.number_format($rowEmp->precio,2,".",",").'</th>
            </tr>';
}

$html .= '<!--<tr>
            <th colspan="3" style="background-color: #D4CDCC;"></th>
          </tr>-->
            <tr>
              <th colspan="3" align="center" style="font-size: 9"></th>
            </tr>
          </table>
            <table border="0" style="margin-top:0;margin-bottom:0;">
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Subtotal</th>
                    <th style="font-size: 7" align="center">$'.number_format($GLOBALS['subtotal'],2,".",",").'</th>
                </tr>
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Descuento</th>
                    <th style="font-size: 7" align="center">$'.number_format($GLOBALS['descuento'],2,".",",").'</th>
                </tr>
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Total</th>
                    <th style="font-size: 7; font-weight: bold;" align="center">$'.number_format($GLOBALS['monto_total'],2,".",",").'</th>
                </tr>';
                if($pagacon!='' || $pagacon>0){
                  if($pagoe==0 && $pagot>0 || $pagoe>0 && $pagot==0){ //paga con 1 solo metodo 
                    //log_message('error', '1 solo pago, pagocon dif a vacio o mayoy a 0 - pagot: '.$pagot);
                    //log_message('error', '1 solo pago, pagocon dif a vacio o mayoy a 0  - pagoe: '.$pagoe);
                    $html .= '
                    <tr>
                      <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                      <th style="font-size: 5;" align="center"><i>'.$metodo.'</i></th>
                    </tr>
                    <tr>
                      <th style="font-size: 6; font-weight: bold;" align="center"></th>
                      <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                      <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($pagacon,2,".",",").'</th>
                    </tr>';
                  }else if($pagoe>0 && $pagot>0 && $pagacon!='' || $pagoe>0 && $pagot>0 && $pagacon>0){
                    $html .= '
                      <tr>
                        <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                        <th style="font-size: 5;" align="center"><i>Efectivo</i></th>
                      </tr>
                      <tr>
                        <th style="font-size: 6; font-weight: bold;" align="center"></th>
                        <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                        <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($pagoe,2,".",",").'</th>
                      </tr>
                      <tr>
                        <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                        <th style="font-size: 5;" align="center"><i>Tarjeta</i></th>
                      </tr>
                      <tr>
                        <th style="font-size: 6; font-weight: bold;" align="center"></th>
                        <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                        <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($pagot,2,".",",").'</th>
                      </tr>';
                  }
                }
                else if($pagacon==''){
                  if($pagoe==0 && $pagot>0 || $pagoe>0 && $pagot==0){ //paga con 1 solo metodo 
                    //log_message('error', '1 solo pago - pagot: '.$pagot);
                    //log_message('error', '1 solo pago - pagoe: '.$pagoe);
                    $html .= '
                    <tr>
                      <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                      <th style="font-size: 5;" align="center"><i>'.$metodo.'</i></th>
                    </tr>
                    <tr>
                      <th style="font-size: 6; font-weight: bold;" align="center"></th>
                      <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                      <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format(0,2,".",",").'</th>
                    </tr>';
                    $pagacon="";
                  }else if($pagoe>0 && $pagot>0){
                    //log_message('error', '2 pagos - pagot: '.$pagot);
                    //log_message('error', '2 pagos - pagoe: '.$pagoe);
                    $html .= '
                      <tr>
                        <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                        <th style="font-size: 5;" align="center"><i>Efectivo</i></th>
                      </tr>
                      <tr>
                        <th style="font-size: 6; font-weight: bold;" align="center"></th>
                        <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                        <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($pagoe,2,".",",").'</th>
                      </tr>
                      <tr>
                        <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                        <th style="font-size: 5;" align="center"><i>Tarjeta</i></th>
                      </tr>
                      <tr>
                        <th style="font-size: 6; font-weight: bold;" align="center"></th>
                        <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                        <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($pagot,2,".",",").'</th>
                      </tr>';
                  }
                }
                else if($pagacon==0){
                  //log_message('error', 'pago con =0: '.$pagacon);
                  $html .= '
                  <tr>
                    <th colspan="2" style="font-size: 5;" align="right"><i>Método de pago:</i></th>
                    <th style="font-size: 5;" align="center"><i>'.$metodo.'</i></th>
                  </tr>
                  <tr>
                    <th style="font-size: 6; font-weight: bold;" align="center"></th>
                    <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                    <th style="font-size: 6; font-weight: bold;" align="center">Plataforma</th>
                  </tr>';
                }if($metodo==4){
                  //log_message('error', 'metodo = 4: '.$metodo);
                  $html .= '<tr>
                    <th style="font-size: 6; font-weight: bold;" align="center"></th>
                    <th style="font-size: 6; font-weight: bold;" align="center">Su pago:</th>
                    <th style="font-size: 6; font-weight: bold;" align="center">Plataforma</th>
                  </tr>';
                }
                if($cambio!='' || $pagacon>0){
                  //log_message('error', 'cambio != 0: '.$metodo);
                  //log_message('error', 'o pagacon > 0: '.$metodo);
                  $html .= '<tr>
                    <th style="font-size: 6; font-weight: bold;" align="center"></th>
                    <th style="font-size: 6; font-weight: bold;" align="center">CAMBIO</th>
                    <th style="font-size: 6; font-weight: bold;" align="center">$'.number_format($cambio,2,".",",").'</th>
                  </tr>';
                }
                
                $html .= '
                <tr>
                  <br>
                  <th colspan="3"><hr style="background-color: #908D8D; height:5px;"></th>
                </tr>
                <tr>
                  <th colspan="3"></th>
                </tr>
                <tr>
                  <th colspan="3" align="Center" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['mensaje2'].'</th>
                </tr>
                <tr>
                  <th colspan="3" align="center" style="font-size: 5">mangoo.mx | Fábrica de Software</th>
                </tr>
                </table>';
      $pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>