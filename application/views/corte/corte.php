<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
    <div class="col-md-12">
      <h2>Corte de caja </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Corte de caja</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row inputbusquedas">
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-1">Desde:</label>
                                <div class="col-md-2">
                                    <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                                </div>
                                <label class="control-label col-md-1">Hasta:</label>
                                <div class="col-md-2">
                                    <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                                </div>
                                <div class="col-md-2">                      
                                    <div class="checkbox-list">
                                        <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                                    </div>
                                </div>
                                <div class="col-md-2">   
                                    <div class="form-group">
                                        <label class="control-label">Tipo de Venta:</label>
                                        <select class="form-control" id="tipo_costo">
                                            <option value="4">Todos</option>
                                            <option value="1">Mostrador</option>
                                            <option value="2">Mesa</option>
                                            <option value="3">A domicilio</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-raised gradient-purple-bliss white" id="btnBuscar">Buscar</a>
                                    <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"   ><i class="fa fa-print"></i></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="imprimir">
                    <div class="col-md-12" id="tbCorte">
                        
                    </div>
                    <div class="col-md-12" id="tbCorte2">
                        
                    </div>
                    <div class="col-md-6">
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">TOTAL:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dTotal">0.00</span>
                            </span>
                        </p>
                        
                        
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total de ventas:</span>
                            <span class="col-md-4" >
                                <span class="text-warning"> </span>
                                <span id="rowventas">0</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total de utilidad:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="totalutilidades">0</span>
                            </span>
                        </p>
                    </div>
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function imprimir(){
      window.print();
    }
</script>