<div class="row">
                <div class="col-md-12">
                  <h2>Lista Turnos</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Turnos</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables">
                                      <thead>
                                        <tr>
                                          <th>Fecha</th>
                                          <th>Hora inicio</th>
                                          <th>Hora Cierre</th>
                                          <th>Nombre</th>
                                          <th>Estatus</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <?php foreach ($lturnos->result() as $item){ ?>
                                         <tr id="trven_<?php echo $item->id; ?>">
                                                  <td><?php echo $item->fecha; ?></td>
                                                  <td><?php echo $item->horaa; ?></td>
                                                  <td><?php echo $item->horac; ?></td>
                                                  <td><?php echo $item->nombre; ?></td>
                                                  <td><?php echo $item->status; ?></td>
                                                  <td>
                                                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="consultarturn(<?php echo $item->id; ?>)">
                                                      <i class="fa fa-book"></i>
                                                    </button>
                                                  </td>
                                          </tr>
                                          
                                        <?php } ?>
                                            
                                      </tbody>
                                    </table>
                                    <div class="col-md-12">
                                      <div class="col-md-9">
                                        
                                      </div>
                                      <div class="col-md-3">
                                        <?php echo $this->pagination->create_links() ?>
                                      </div>
                                      
                                    </div>
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<script type="text/javascript">
  function consultarturn(id){
    $("#iframeri").modal();
    $.ajax({
      type:'POST',
      url:'Ventas/consultarturno',
      data:{id:id},
      async:false,
      success:function(data){
        //var array = $.parseJSON(data);
        $('#tbCorte').html(data);
        
      }
    });
    
  }
</script>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ventas del Turno</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Productos vendidos en el turno</p>
                <div class="row" align="center" >
                  <div class="col-md-12" id="tbCorte">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>