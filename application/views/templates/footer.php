    </div>
        </div>

        <footer class="footer footer-static footer-light">
          <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; <?php echo date("Y"); ?> <a href="http://www.mangoo.mx" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">Mangoo Software </a>, All rights reserved. </span></p>
        </footer>

      </div>
    </div>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pGenerator.jquery.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/gmaps.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/legacy.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/toastr.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN APEX JS-->
    <script src="<?php echo base_url(); ?>app-assets/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/tooltip.js" type="text/javascript"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!--<script src="<?php echo base_url(); ?>app-assets/js/dashboard1.js" type="text/javascript"></script>-->

    <script src="<?php echo base_url(); ?>/app-assets/js/components-modal.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>public/plugins/jquery.confirm/jquery-confirm.js"></script>

    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/adapters/jquery.js'></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $( '[data-rel^="ckeditor"]' ).ckeditor();
            $('#updatenotas').click(function(){
                $.ajax({
                    type:'POST',
                    url: '<?php echo base_url(); ?>Inicio/notas',
                    data: {
                        user: $('#notasedit2').val(),
                        nota: $('#notass').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('nota actualizada','Hecho!');
                    }
                });
            });
        });
        
    </script>
    <!-- END PAGE LEVEL JS-->
  </body> 
</html> 