<div class="row">
                <div class="col-md-12">
                  <h2>Clientes</h2>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-11"></div>
                  <div class="col-md-1">
                    <a href="<?php echo base_url(); ?>Clientes/clientesadd" class="btn btn-raised gradient-brady-brady-fun-fun white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Clientes</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                          <div class="col-md-3">
                              <form role="search" class="navbar-form navbar-right mt-1">
                                <div class="position-relative has-icon-right">
                                  <input type="text" placeholder="Buscar" id="buscarcli" class="form-control round" oninput="buscarcliente()">
                                  <div class="form-control-position"><i class="ft-search"></i></div>
                                </div>
                              </form>
                          </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Telefono</th>
                                          <th>Municipio</th>
                                          <th>Direccion</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadoscli">

                                        <?php foreach ($clientes->result() as $item){ ?>
                                         <tr id="trcli_<?php echo $item->ClientesId; ?>">
                                                  <td><?php echo $item->Nom; ?></td>
                                                  <td><?php echo $item->telefonoc; ?></td>
                                                  <td><?php echo $item->Municipio; ?></td>
                                                  <td><?php echo $item->Calle; ?> <?php echo $item->noExterior; ?> <?php echo $item->Localidad; ?> <?php echo $item->Municipio; ?> <?php echo $item->Estado; ?></td>
                                                  <td>
                                                      <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                                                      <button type="button" class="btn btn-raised btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          <span class="sr-only">Toggle Dropdown</span>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                          <a class="dropdown-item" href="<?php echo base_url();?>Clientes/Clientesadd?id=<?php echo $item->ClientesId; ?>">Editar</a>
                                                          <a class="dropdown-item" onclick="clientesdelete(<?php echo $item->ClientesId; ?>);"href="#">Eliminar</a>
                                                      </div>
                                                  </div>




                                                    
                                                  </td>
                                             
                                                </tr>
                                          
                                        <?php } ?>
                                      <table class="table table-striped" id="data-tables2" style="display: none; width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Telefono</th>
                                          <th>Municipio</th>
                                          <th>Direccion</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadoscli2">  
                                      </tbody>
                                    </table>
                                    </tbody>
                                    </table>
                                    <div class="col-md-12">
                                      <div class="col-md-7">
                                        
                                      </div>
                                      <div class="col-md-5">
                                        <?php echo $this->pagination->create_links() ?>
                                      </div>
                                      
                                    </div>
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<script type="text/javascript">
        $(document).ready(function() {
                $('#data-tabless').dataTable();
        } );
        function clientesdelete(id){
          $.ajax({
                  type:'POST',
                  url: '<?php echo base_url(); ?>Clientes/deleteclientes',
                  data: {id:id},
                  async: false,
                  statusCode:{
                      404: function(data){
                          toastr.error('Error!', 'No Se encuentra el archivo');
                      },
                      500: function(){
                          toastr.error('Error', '500');
                      }
                  },
                  success:function(data){
                      console.log(data);
                      //location.reload();
                      toastr.success('Hecho!', 'eliminado Correctamente');
                      var row = document.getElementById('trcli_'+id);
                                  row.parentNode.removeChild(row);
                      
                  }
              });
      }
</script>