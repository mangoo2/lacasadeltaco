<?php 
if (isset($_GET['id'])) {
  $id=$_GET['id'];
  $clientesv=$this->ModeloClientes->getcliente($id);
  foreach ($clientesv->result() as $item){
    $label='Editar Cliente';
    $id = $item->ClientesId;
    $Nom = $item->Nom;
    $Calle = $item->Calle;
    $noExterior = $item->noExterior;
    $Colonia = $item->Colonia;
    //$Localidad = $item->Localidad;
    $Municipio = $item->Municipio;
    $Estado = $item->Estado;
    //$Pais = $item->Pais;
    //$CodigoPostal = $item->CodigoPostal;
    $Correo = $item->Correo;
    $noInterior = $item->noInterior;
    $nombrec = $item->nombrec;
    $correoc = $item->correoc;
    $telefonoc = $item->telefonoc;
    $extencionc = $item->extencionc;
    $nextelc = $item->nextelc;
    $descripcionc = $item->descripcionc;
    $referencia = $item->referencia;
  }
}else{
  $label='Nuevo Cliente';
  $id = 0;
  $Nom = '';
  $Calle = '';
  $noExterior = '';
  $Colonia = '';
  //$Localidad = '';
  $Municipio = '';
  $Estado = '';
  //$Pais = 'México';
  //$CodigoPostal = '';
  $Correo = '';
  $noInterior = '';
  $nombrec = '';
  $correoc = '';
  $telefonoc = '';
  $extencionc = '';
  $nextelc = '';
  $descripcionc = ''; 
  $referencia = '';    
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
</style>
<input type="hidden" name="clienteid" id="clienteid" value="<?php echo $id;?>">
<div class="row">
  <div class="col-md-12">
    <h2>Cliente</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php echo $label;?></h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post"  role="form" id="formclientes">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Nombre:</label>
                            <div class=" col-md-10 input-group">
                              <input type="text" class="form-control" id="nombrecli" name="nombrecli" value="<?php echo $Nom;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Correo:</label>
                            <div class=" col-md-10 input-group">
                              <input type="email" class="form-control" id="correocli" name="correocli" value="<?php echo $Correo;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <h3>Domicilio</h3>
                          <hr />
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Calle:</label>
                              <div class=" col-md-9 input-group">
                                <input type="text" class="form-control" id="callecli" name="callecli" value="<?php echo $Calle;?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label class="col-md-7 control-label">No Exterior:</label>
                              <div class=" col-md-5 input-group">
                                <input type="text" class="form-control" id="nexteriorcli" name="nexteriorcli" value="<?php echo $noExterior;?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label class="col-md-7 control-label">No Interior:</label>
                              <div class=" col-md-5 input-group">
                                <input type="text" class="form-control" id="ninteriorcli" name="ninteriorcli" value="<?php echo $noInterior;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Colonia:</label>
                              <div class=" col-md-9 input-group">
                                <input type="text" class="form-control" id="coloniacli" name="coloniacli" value="<?php echo $Colonia;?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Municipio:</label>
                              <div class=" col-md-9 input-group">
                                <input type="text" class="form-control" id="municipiocli" name="municipiocli" value="<?php echo $Municipio;?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Estado:</label>
                              <div class=" col-md-9 input-group">
                                <input type="text" class="form-control" id="estadocli" name="estadocli" value="<?php echo $Estado;?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Referencia:</label>
                              <div class=" col-md-9 input-group">
                                <textarea rows="5" class="form-control" id="referencia" name="referencia"><?php echo $referencia;?></textarea>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <h3>Contacto Directo</h3>
                          <hr />
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-md-3 control-label">Nombre contacto:</label>
                                <div class=" col-md-9 input-group">
                                  <input type="text" class="form-control" id="contactocli" name="contactocli" value="<?php echo $nombrec;?>">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-md-3 control-label">Correo:</label>
                                <div class=" col-md-9 input-group">
                                  <input type="text" class="form-control" id="correoccli" name="correoccli" value="<?php echo $correoc;?>">
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-md-3 control-label">Teléfono:</label>
                                <div class=" col-md-9 input-group">
                                  <input type="text" class="form-control" id="telefonocli" name="telefonocli" value="<?php echo $telefonoc;?>">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-md-3 control-label">Extención:</label>
                                <div class=" col-md-9 input-group">
                                  <input type="text" class="form-control" id="extenciocli" name="extenciocli" value="<?php echo $extencionc;?>">
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-md-3 control-label">Nextel:</label>
                                <div class=" col-md-9 input-group">
                                  <input type="text" class="form-control" id="nextelcli" name="nextelcli" value="<?php echo $nextelc;?>">
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Descripción:</label>
                              <div class=" col-md-9 input-group">
                                <textarea class="form-control" id="descripcioncli" name="descripcioncli"><?php echo $descripcionc;?></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                    </form>
                    <div class="col-md-12">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"  id="savecl">Guardar</a>
                    </div>
                  </div>
                  <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>