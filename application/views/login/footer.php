    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/jquery.validate.js"></script>

    <!-- Custom Js -->
    <!--<script src="https://carta-digital.mx/sistema/public/js/login.js"></script>-->
    <script type="text/javascript">
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('<?php echo base_url();?>service-worker.js')
            .then(function(registration) {
              // Si es exitoso
              console.log('SW registrado correctamente');
            }, function(err) {
              // Si falla
              console.log('SW fallo', err);
            });
          });
        }
    </script>
</body>

</html>