<!DOCTYPE html>
<html lang="es">

<head>

    <meta name="theme-color" content="#f5f7fa" />
    <meta name="Description" content="sistema punto de venta">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>La Casa del Taco - Mangoo Software POS</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>public/img/ico/LaCasaDelTaco_H.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>app-assets/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>app-assets/css/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>app-assets/css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>app-assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>manifest.json" rel="manifest">
</head>
<body class="login-page">