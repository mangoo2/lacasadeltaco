<div class="row">
    <div class="col-md-12">
      <h2>Categorias </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Categorias</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <!--------//////////////-------->
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" name="categoria" id="categoria" class="form-control">
                        </div>
                        <div class="col-md-8">
                            <button type="button" id="categoriaadd" class="btn btn-raised gradient-purple-bliss white">Agregar</button>
                        </div>
                        <div class="col-md-8">
                            <table class="table table-striped" id="data-tables">
                                      <thead>
                                        <tr>
                                          <th>Categoria</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <?php foreach ($categoriasll->result() as $item){ ?>
                                         <tr id="trven_<?php echo $item->categoriaId; ?>">
                                                  <td><?php echo $item->categoria; ?></td>
                                                  
                                                  <td>
                                                    
                                                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="categodelete(<?php echo $item->categoriaId; ?>)" title="Eliminar" data-toggle="tooltip" data-placement="top">
                                                      <i class="fa fa-times"></i>
                                                    </button>
                                                  </td>
                                          </tr>
                                          
                                        <?php } ?>
                                            
                                      </tbody>
                                    </table>
                        </div>
                    </div>
                    
                    
                    <!--------//////////////-------->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function categodelete(id){
      $.ajax({
        type:'POST',
        url:'Categoria/categoriadell',
        data:{id:id},
        async:false,
        success:function(data){
          toastr.success('Eliminado Correctamente','Hecho!');
          $('#trven_'+id).remove();
        }
      });
    
    }
    $(document).ready(function () {
        $('#categoriaadd').click(function(event) {
            
                $.ajax({
                    type:'POST',
                    url: 'Categoria/categoriaadd',
                    data: {
                        nom: $('#categoria').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Guardado Correctamente','Hecho!');
                        location.href=''; 
                        window.location.href = "";
                    }
                });
            });
    });
    
        
</script>