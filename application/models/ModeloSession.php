<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "CALL SP_GET_SESSION('$usu');";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;  
            
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $_SESSION['usuarioid_tz']=$id;
                $_SESSION['usuario_tz']=$nom;
                $_SESSION['perfilid_tz']=$perfil;
                $_SESSION['idpersonal_tz']=$idpersonal;
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        
        echo $count;
    }
    public function menus($perfil){
        //$strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon 
                from menu as men, menu_sub as mens, personal_menu as perfd 
                where men.MenuId=mens.MenuId and perfd.MenuId=mens.MenusubId and perfd.personalId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
                from menu_sub as menus, personal_menu as perfd 
                WHERE perfd.MenuId=menus.MenusubId and perfd.personalId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    function alertproductos(){
        $strq = "SELECT count(*) as total FROM productos where stock<=stockmin and activo=1 and stockok=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function alertproductosall(){
        $strq = "SELECT * FROM productos where stock<=stockmin and activo=1 and stockok=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function alertinsumos(){
        $strq = "SELECT count(*) as total FROM insumos where existencia<=stockmin and activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function alertinsumosall(){
        $strq = "SELECT * FROM insumos where existencia<=stockmin and activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function notas(){
        $strq = "SELECT * FROM notas";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

}
