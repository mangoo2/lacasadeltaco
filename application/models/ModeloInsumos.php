<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloInsumos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlistinsumos($params){
        $columns = array(
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'operaciones',
            4=>'stockmin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('insumos');
        $this->db->where(array('activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistinsumost($params){
        $columns = array(
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'operaciones',
            4=>'stockmin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('insumos');
        $this->db->where(array('activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                    $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function insumoSearch($ins){
        $strq = "SELECT *, insumos.insumo as nom_insumo 
            FROM insumos 
            /*join productos_insumos ps on ps.insumo=insumos.insumosId
            join productos p on p.productoid=ps.productoId*/
            where insumos.activo=1 and insumos.insumo like '%".$ins."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function insumoSearch2($ins){
        $strq = "SELECT * 
            FROM productos p
            where activo=1 and nombre like '%".$ins."%'
            or activo=1 and codigo like '%".$ins."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getInsumo($id){
        $strq = "SELECT * FROM insumos where insumosId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

}