<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Productosall extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloProductos');
    }
	public function index(){
		$data['getventasd']=$this->ModeloProductos->productosall();
		$data['total_rows'] = $this->ModeloProductos->filas();
        $data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
        $data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
        $data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra();
		$this->load->view('Reportes/productosall',$data);
	        
	}
}