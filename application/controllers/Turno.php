<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turno extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            $data['sturno']=$this->ModeloVentas->turnoss();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('turno/turno',$data);
            $this->load->view('templates/footer');
            $this->load->view('turno/jsturno');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $resultadoc=$this->ModeloVentas->corte($inicio,$fin);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin);

        $table="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>Numero venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Efectivo</th>
                            <th>Tarjeta</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($resultadoc->result() as $fila) {
            $table .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$ ".$fila->subtotal."</td>
                            <td>$ ".$fila->monto_total."</td>
                            <td>$ ".$fila->efectivo."</td>
                            <td>$ ".$fila->pagotarjeta."</td>
                        </tr>";
        }
        $table.="</tbody> </table>";
        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
        }




        $array = array("tabla"=>$table,
                        "dTotal"=>round($total,2),
                        "dImpuestos"=>0,
                        "dSubtotal"=>round($subtotal,2),
                    );
            echo json_encode($array);
    }
    

       
    
}
