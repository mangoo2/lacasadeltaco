<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ticket extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->helper('url');
        //$this->load->model('Solicitud');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$id=$this->input->get('id');
		
		if (isset($_GET['vcambio'])) {
			$data['vcambio']=$_GET['vcambio'];
			$data['vpagacon']=$_GET['vpagacon'];
			$data['metodo']=$_GET['metodo'];
		}else{
			$data['vcambio']='';
			$data['vpagacon']='';
			$data['metodo']='';
		}

		$data['configticket']=$this->ModeloVentas->configticket();
		$data['getventas']=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);
		$this->load->view('Reportes/ticket',$data);
	        
	}
}