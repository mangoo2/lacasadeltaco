<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Etiquetas extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloProductos');
    }
	public function index(){
		$id=$this->input->get('id');
		$data['getventasd']=$this->ModeloProductos->getproducto($id);
		$this->load->view('Reportes/etiqueta',$data);
	        
	}
}