<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insumos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloInsumos');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            

            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('insumos/insumoslis');
            $this->load->view('templates/footer');
            $this->load->view('insumos/insumoslisjs');
	}
    public function insumosadd($id=0){
        
        $insumosId=0;
        $insumo='';
        $existencia='';
        $stockmin='';
        $operaciones='';
        $data['label']='Nuevo Insumo';
        $resultpro=$this->ModeloCatalogos->getselectwheren('insumos',array('insumosId'=>$id));
        foreach ($resultpro->result() as $item) {
            $insumosId=$item->insumosId;
            $insumo=$item->insumo;
            $existencia=$item->existencia;
            $stockmin=$item->stockmin;
            $data['label']='Editar Insumo';
        }

        $data['insumosId']=$insumosId;
        $data['insumo']=$insumo;
        $data['existencia']=$existencia;
        $data['stockmin']=$stockmin;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('insumos/insumoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('insumos/insumoaddjs');
    }
    function insadd(){
        $params=$this->input->post();
        $insumosId=$params['insumosId'];
        unset($params['insumosId']);
        
        if ($insumosId>0) {
            $this->ModeloCatalogos->updateCatalogo('insumos',$params,array('insumosId'=>$insumosId));
        }else{
            $insumosId=$this->ModeloCatalogos->Insert('insumos',$params);
        }
    }
    
  
    public function deleteinsumos(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('insumos',array('activo'=>0),array('insumosId'=>$id));
    }
    
    
    public function getlistinsumo() {
        $params = $this->input->post();
        $getdata = $this->ModeloInsumos->getlistinsumos($params);
        $totaldata= $this->ModeloInsumos->getlistinsumost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
       
    
}
