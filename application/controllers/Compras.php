<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloInsumos');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras');
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    public function searchInsumo(){
        $ins = $this->input->get('search');
        $results=$this->ModeloInsumos->insumoSearch($ins);
        echo json_encode($results->result());
    }

    public function searchProd(){
        $ins = $this->input->get('search');
        $results=$this->ModeloInsumos->insumoSearch2($ins);
        echo json_encode($results->result());
    }

    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $tipo = $this->input->post('tipo');
        if($tipo==0){
            $personalv=$this->ModeloProductos->getproducto($prod);
            foreach ($personalv->result() as $item){
                $id = $item->productoid;
                $codigo = $item->codigo;
                $nombre = $item->nombre;
                $precio = $item->preciocompra;
            }
        }else{
            $personalv=$this->ModeloInsumos->getInsumo($prod);
            foreach ($personalv->result() as $item){
                $id = $item->insumosId;
                $codigo = "";
                $nombre = $item->insumo;
                $precio = 0;
            }
        }
        /*log_message('error', 'id: '.$id);
        log_message('error', 'codigo: '.$codigo);
        log_message('error', 'nombre: '.$nombre);
        log_message('error', 'precio: '.$precio);*/

        $array = array("id"=>$id,
                        "codigo"=>$codigo,
                        "cant"=>$cant,
                        "producto"=>$nombre,
                        "precio"=>$prec
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $total = $this->input->post('total');
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$total);
        //$id=8;
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            //if(isset($DATA[$i]->insumo)){
                $insumo = $DATA[$i]->insumo;
            //}
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            //if(isset($DATA[$i]->cant_env)){
                $cant_env = $DATA[$i]->cant_env;
            //}
            $precio = $DATA[$i]->precio;

            /*log_message('error', 'insumo: '.$insumo);
            log_message('error', 'producto: '.$producto);
            log_message('error', 'cantidad: '.$cantidad);
            log_message('error', 'cant_env: '.$cant_env);
            log_message('error', 'precio: '.$precio);*/

            /*$get_ok=$this->ModeloCatalogos->getselectwheren("productos",array("productoid"=>$producto));
            foreach ($get_ok->result() as $key ) {
                $stockok=$key->stockok;
            }
            log_message('error', 'stockok: '.$stockok);*/
            if($insumo==0){ //producto de empaque
                $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$precio);
            }else{ //insumo
                $tipo=0;
                $this->ModeloVentas->ingresarcompradIns($idcompra,$producto,$cantidad,$precio,$tipo);
                $this->ModeloVentas->ingresarStockInsumo($insumo,$cant_env);
            }
            
            
        }
    }

}