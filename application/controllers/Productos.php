<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
    }
	public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('productos/productos');
            $this->load->view('templates/footer');
            $this->load->view('productos/jsproducto');
	}
    public function productosadd($id=0){
        
        $productoid=0;
        $codigo='';
        $nombre='';
        $stockok='';
        $stock='';
        $stockmin='';
        $preciocompra='';
        /*$preciocomp_uber='';
        $preciocomp_rappi='';
        $preciocomp_didi='';*/
        $preciopos='';
        /*$preciouber='';
        $preciorappi='';
        $preciodid='';*/
        $tipo='';
        $cant_tot_prods='';
        $data['label']='Nuevo Producto';
        $resultpro=$this->ModeloCatalogos->getselectwheren('productos',array('productoid'=>$id));
        foreach ($resultpro->result() as $item) {
            $productoid=$item->productoid;
            $codigo=$item->codigo;
            $nombre=$item->nombre;
            $stockok=$item->stockok;
            $stock=$item->stock;
            $stockmin=$item->stockmin;
            $preciocompra=$item->preciocompra;
            /*$preciocomp_uber=$item->preciocomp_uber;
            $preciocomp_rappi=$item->preciocomp_rappi;
            $preciocomp_didi=$item->preciocomp_didi;*/
            $preciopos=$item->preciopos;
            /*$preciouber=$item->preciouber;
            $preciorappi=$item->preciorappi;
            $preciodid=$item->preciodid;*/
            $tipo=$item->tipo;
            $cant_tot_prods=$item->cant_tot_prods;
            $data['label']='Editar Producto';
        }

        $data['productoid']=$productoid;
        $data['codigo']=$codigo;
        $data['nombre']=$nombre;
        $data['stockok']=$stockok;
        $data['stock']=$stock;
        $data['stockmin']=$stockmin;
        $data['preciocompra']=$preciocompra;
        /*$data['preciocomp_uber']=$preciocomp_uber;
        $data['preciocomp_rappi']=$preciocomp_rappi;
        $data['preciocomp_didi']=$preciocomp_didi;*/
        $data['preciopos']=$preciopos;
        /*$data['preciouber']=$preciouber;
        $data['preciorappi']=$preciorappi;
        $data['preciodid']=$preciodid;*/
        $data['tipo']=$tipo;
        $data["cant_tot_prods"]=$cant_tot_prods;

        $data["cats"]=$this->ModeloCatalogos->getselectwheren('categoria',array('activo'=>1));
        /*if($tipo==0) // producto simple
            $data["categos"]=$this->ModeloProductos->getCategosProds($id,$tipo);
        else if($tipo==1) //combo */
            $data["categos"]=$this->ModeloProductos->getCategosProdsCombo($id,$tipo);

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/productoaddjs');
    }
    function productoadd(){
        $params=$this->input->post();
        $productoid=$params['productoid'];
        $arrarinsumos =$params['arrarinsumos'];
        $arrarcats =$params['arrarcats'];
        $tipo =$params['tipo'];
        $cant_total =$this->input->post("cant_tot_prods");
        $params["cant_tot_prods"]=$cant_total;
        unset($params['productoid']);
        unset($params['arrarinsumos']);
        unset($params['arrarcats']);
        //unset($cant_total['cant_total']);
        if($params['stockok']=='on'){
            $params['stockok']=1;
        }else{
            $params['stockok']=0;
        }
        
        if ($productoid>0) {
            $this->ModeloCatalogos->updateCatalogo('productos',$params,array('productoid'=>$productoid));
        }else{
            $productoid=$this->ModeloCatalogos->Insert('productos',$params);
        }

        $DATAc = json_decode($arrarinsumos);       
        for ($i=0;$i<count($DATAc);$i++) {
            if ($DATAc[$i]->insumoId>0 && $DATAc[$i]->tipo==0) {
                $dataarray = array(
                                'cantidad'=>$DATAc[$i]->cantidad,
                                'insumo'=>$DATAc[$i]->insumo
                                );
                $this->ModeloCatalogos->updateCatalogo('productos_insumos',$dataarray,array('insumoId'=>$DATAc[$i]->insumoId));
            }else if($DATAc[$i]->insumoId==0 && $DATAc[$i]->tipo==0){
                $dataarray = array(
                                'productoid'=>$productoid,
                                'cantidad'=>$DATAc[$i]->cantidad,
                                'insumo'=>$DATAc[$i]->insumo
                                );
                $this->ModeloCatalogos->Insert('productos_insumos',$dataarray);
            }
            if($DATAc[$i]->insumoId>0 && $DATAc[$i]->tipo==1) {
                $dataarray = array(
                                'cantidad'=>$DATAc[$i]->cantidad,
                                'id_producto_combo'=>$DATAc[$i]->insumo
                                );
                $this->ModeloCatalogos->updateCatalogo('productos_combo',$dataarray,array('id'=>$DATAc[$i]->insumoId));
            }else if($DATAc[$i]->insumoId==0 && $DATAc[$i]->tipo==1){
                $dataarray = array(
                                'id_producto'=>$productoid,
                                'cantidad'=>$DATAc[$i]->cantidad,
                                'id_producto_combo'=>$DATAc[$i]->insumo
                                );
                $this->ModeloCatalogos->Insert('productos_combo',$dataarray);
            }
        }

        $DATACa = json_decode($arrarcats);       
        for ($i=0;$i<count($DATACa);$i++) {
            if ($DATACa[$i]->id>0) {
                $dataarrayc = array(
                                'id_producto'=>$productoid,
                                'cantidad'=>$DATACa[$i]->cantidad,
                                'id_categoria'=>$DATACa[$i]->id_categoria,
                                'fecha_reg'=>date("Y-m-d H:i:s"),
                                'tipo'=>$tipo,
                                //'cant_tot_prods'=>$cant_total
                                );
                $this->ModeloCatalogos->updateCatalogo('productos_categos_combos',$dataarrayc,array('id'=>$DATACa[$i]->id));
            }else {
                $dataarrayc = array(
                                'id_producto'=>$productoid,
                                'cantidad'=>$DATACa[$i]->cantidad,
                                'id_categoria'=>$DATACa[$i]->id_categoria,
                                'fecha_reg'=>date("Y-m-d H:i:s"),
                                'tipo'=>$tipo,
                                //'cant_tot_prods'=>$cant_total
                                );
                $this->ModeloCatalogos->Insert('productos_categos_combos',$dataarrayc);
            }
        }
    }
    /*
    function imgpro(){
        $idpro = $this->input->post('idpro');
        $upload_folder ='public/img/productos';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $tipo_archivo1 = explode('/', $tipo_archivo); 
        $tipoactivo=$tipo_archivo1[1];
        if ($tipoactivo!='png') {
            $tipo_archivo1 = explode('+', $tipoactivo); 
            $tipoactivo=$tipo_archivo1[0];
        }
        $archivador = $upload_folder . '/'.$fecha.'pro.'.$tipoactivo;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $resimg=$this->ModeloProductos->imgpro($archivador,$idpro);
            $return = Array('ok'=>TRUE,'img'=>$tipoactivo);
        }
        echo json_encode($return);
    }*/
    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }
    
    public function getlistproductos() {
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistproductos($params);
        $totaldata= $this->ModeloProductos->getlistproductost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function searchinsumo(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProductos->insumossearch($usu);
        echo json_encode($results->result());
    }
    public function searchProducto(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProductos->procutossearch($usu);
        echo json_encode($results->result());
    }
    function datospro(){
        $pro=$this->input->post('pro');
        $tipo=$this->input->post('tipo');
        if($tipo==0){
            $strq = "SELECT proi.insumoId,proi.cantidad,ins.insumosId,ins.insumo, $tipo as tipo
                    FROM productos_insumos as proi
                    inner join insumos as ins on ins.insumosId=proi.insumo
                    where proi.activo=1 and proi.productoid=$pro";
        }else{
            $strq = "SELECT pc.id, pc.id_producto,pc.id_producto_combo,pc.cantidad, p.tipo, p.codigo, p.nombre, concat(p.codigo,' - ',p.nombre) as prod_name
                    FROM productos_combo as pc
                    inner join productos as p on p.productoid=pc.id_producto_combo
                    where pc.activo=1 and pc.id_producto=$pro";
        }
        $query = $this->db->query($strq);
        $results=$query->result();
        echo json_encode($results);
    }
    public function deletecategos(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('productos_categos_combos',array('estatus'=>0),array('id'=>$id));
    }

    public function deleteinsumos(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('productos_insumos',array('activo'=>0),array('insumoId'=>$id));
    }
    public function deleteprods(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('productos_combo',array('activo'=>0),array('id'=>$id));
    }  

    public function getCategoProds(){
        $id = $this->input->post('id');
        $tipo = $this->input->post('tipo');
        $categos=$this->ModeloProductos->getCategosProdsCombo($id,$tipo);
        foreach ($categos as $c) { 
            echo '<tr class="pro_si_catego_'.$c->id.'">
              <td>Cantidad de categorías</td>
              <td>
                <input type="hidden" class="form-control" id="tipo" value="'.$c->tipo.'">
                <input type="hidden" class="form-control" id="id_cat_prod" value="'.$c->id.'">
                <input type="hidden" class="form-control" id="id_categoria" value="'.$c->id_categoria.'">
                <input type="number" class="form-control" min="1" id="cantidad" value="'.$c->cantidad.'" style="max-width: 100px">
              </td>
              <td>'.$c->categoria.'</td>
              <td>
                <button class="btn btn-raised gradient-flickr white sidebar-shadow"
                  onclick="deletecatego('.$c->id.',0)"
                  data-toggle="tooltip" data-placement="top"
                  data-original-title="Eliminar">
                        <i class="fa fa-times"></i>
                </button>
              </td>
            </tr>';
        }
    }
    
}
