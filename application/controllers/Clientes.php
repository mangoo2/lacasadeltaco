<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
    }
	public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Clientes/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloClientes->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["clientes"] = $this->ModeloClientes->total_paginados($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('clientes/clientes',$data);
            $this->load->view('templates/footer');
            $this->load->view('clientes/jsclientes');
	}
    public function clientesadd(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/clientesadd');
        $this->load->view('templates/footer');
        $this->load->view('clientes/jsclientes');
    }
    function clienteadd(){
        $id = $this->input->post('id');
        $nom = $this->input->post('nom');
        $correo = $this->input->post('correo');
        $calle = $this->input->post('calle');
        $nint = $this->input->post('nint');
        $next = $this->input->post('next');
        $col = $this->input->post('col');
        //$loc = $this->input->post('loc');
        $muni = $this->input->post('muni');
        //$cp = $this->input->post('cp');
        $esta = $this->input->post('esta');
        //$pais = $this->input->post('pais');
        $ref = $this->input->post('referencia');
        $contac = $this->input->post('contac');
        $correoc = $this->input->post('correoc');
        $tel = $this->input->post('tel');
        $ext = $this->input->post('ext');
        $nexte = $this->input->post('nexte');
        $des = $this->input->post('des');
        if ($id>0) {
            $this->ModeloClientes->clientesupdate($id,$nom,$correo,$calle,$nint,$next,$col,$muni,$esta,$contac,$correoc,$tel,$ext,$nexte,$des,$ref);
        }else{
            $this->ModeloClientes->clientesinsert($nom,$correo,$calle,$nint,$next,$col,$muni,$esta,$contac,$correoc,$tel,$ext,$nexte,$des,$ref);
        }
        redirect('/Clientes');
    }
    
    public function deleteclientes(){
        $id = $this->input->post('id');
        $this->ModeloClientes->deleteclientes($id); 
    }
    function buscarcli(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloClientes->clientesallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trcli_<?php echo $item->ClientesId; ?>">
                <td><?php echo $item->Nom; ?></td>
                <td><?php echo $item->telefonoc; ?></td>
                <td><?php echo $item->Municipio; ?></td>
                <td><?php echo $item->Calle; ?> <?php echo $item->noExterior; ?> <?php echo $item->Localidad; ?> <?php echo $item->Municipio; ?> <?php echo $item->Estado; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                        <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                        <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <a class="dropdown-item" href="<?php echo base_url();?>Clientes/Clientesadd?id=<?php echo $item->ClientesId; ?>">Editar</a>
                          <a class="dropdown-item" onclick="clientesdelete(<?php echo $item->ClientesId; ?>);"href="#">Eliminar</a>
                        </div>
                    </div>
                </td> 
            </tr>
        <?php }
    }
    
    

       
    
}
