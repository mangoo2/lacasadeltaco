var base_url = $('#base_url').val();
var table;
$(document).ready(function () {
    table = $('#data-tables').DataTable();
    loadtable();

        var form_register = $('#formproductos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                codigo:{
                  required: true
                },
                nombre:{
                    required: true
                },    
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //=====================================================================================================
        $('#savepr').click(function(event) {
            var varform = $("#formproductos");
            var valid = varform.valid();
            var tipo_prod =0;
            if($("#tipo1").is(":checked")==true){
                tipo_prod=0;
            }else{
                tipo_prod=1;
            }
            if($("#stockok").is(":checked")==true){
                stockok="on";
            }else{
                stockok="off";
            }
            //console.log(valid);
            //============================
                var DATAr  = [];
                var TABLA   = $("#table-insumos tbody > tr");
                    TABLA.each(function(){         
                    item = {};
                    item["tipo"] = tipo_prod;
                    item ["insumoId"] = $(this).find("input[id*='insumoId']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["insumo"]  = $(this).find("select[id*='insumo'] option:selected").val();
                    DATAr.push(item);
                });
                INFO  = new FormData();
                arrarinsumos   = JSON.stringify(DATAr);
            //============================
            //============================
                var DATAC  = [];
                var TABLAC   = $("#table-cats tbody > tr");
                    TABLAC.each(function(){         
                    item = {};
                    item ["id"] = $(this).find("input[id*='id_cat_prod']").val();
                    item ["id_categoria"]   = $(this).find("input[id*='id_categoria']").val();
                    item ["cantidad"]  = $(this).find("input[id*='cantidad']").val();
                    DATAC.push(item);
                });
                INFO  = new FormData();
                arrarcats   = JSON.stringify(DATAC);
            //============================
            if(valid) {
                var datosform=varform.serialize()+'&cant_tot_prods='+$("#cant_tot_prods").val()+'&stockok='+stockok+'&arrarinsumos='+arrarinsumos+"&tipo="+tipo_prod+'&arrarcats='+arrarcats;
                $.ajax({
                    type:'POST',
                    url: base_url+'Productos/productoadd',
                   // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                    data: datosform,
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    beforeSend: function(){
                      $("#savepr").attr("disabled",true);
                    },
                    success:function(data){
                        var idpro=data;
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        //=====================
                        	
                        //=====================
                        setInterval(function(){ 
                        	location.href=base_url+'Productos';
                        }, 3000);  
                    }
                });
            }
        });
        $('#addinsumos').click(function(event) {
            var tipo_p =0;
            if($("#tipo1").is(":checked")==true){
                tipo_p=0;
            }else{
                tipo_p=1;
            }
            addinsumos(0,0,0,'',tipo_p);
        });

        $('#addcategos').click(function(event) {
            addCategos();
        });

    $("#stockok").on("click",function(){
        verStockDis();
    });
    $("#tipo1").on("click",function(){
        verCategos();
    });
    $("#tipo2").on("click",function(){
        verCategos();
    });

    $('#addcategospp').click(function(event) {
        addCategosPP();
    });
    verStockDis();
    verCategos();
});

function verCategos(){
    if($("#tipo2").is(":checked")==true){ //combo
        $("#tipo").val(1);
        tipo_pp=1;
        $("#titlecat").html("CATEGORÍA(S) A LA QUE PERTENECE EL COMBO");
    }else if($("#tipo1").is(":checked")==true){
       $("#tipo").val(0);
       tipo_pp=0;
       $("#titlecat").html("CATEGORÍA(S) A LA QUE PERTENECE EL PRODUCTO");
    }
    $.ajax({
        type:'POST',
        url: base_url+'Productos/getCategoProds',
        data: {id:$("#productoid").val(), tipo:tipo_pp },
        async: false,
        success:function(data){
            $('.btable-cats').html(data);
        }
    });
} 

function verStockDis(){
    if($("#stockok").is(":checked")){
        $("#stock").attr("disabled",false);
        $("#stockmin").attr("disabled",false);
    }
    else{
        $("#stock").attr("disabled",true);
        $("#stockmin").attr("disabled",true);
    }
}
function calcular(){
	var costo = $('#preciocompra').val();
    var porcentaje = $('#porcentaje').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventa').val(cantitotal);
    $('#preciommayoreo').val(cantitotal);
    $('#cpmmayoreo').val('1');
    $('#preciomayoreo').val(cantitotal);
    $('#cpmayoreo').val('1');
}

function loadtable(){
    table.destroy();
    table = $('#data-tables').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Productos/getlistproductos",
            type: "post",
        },
        "columns": [
            {"data": "productoid"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": "preciopos"},
            {"data": "preciocompra"},
            {"data": "stock"},
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+base_url+'Productos/productosadd/'+row.productoid+'">Editar</a>\
                                      <a class="dropdown-item producto_row_'+row.productoid+'"\
                                        onclick="etiquetas('+row.productoid+');"\
                                            data-codigo="'+row.codigo+'" \
                                            data-nombre="'+row.nombre+'" \
                                            data-categoria="" \
                                            data-precioventa="'+row.preciopos+'" \
                                        href="#">Etiquetas</a>\
                                      <a class="dropdown-item" onclick="productodelete('+row.productoid+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
           if (aData.stock<=aData.stockmin){
               $(nRow).addClass('alertastock');
           }
        }
    });
}

var rowcats=0;
var con_cat=0;
function addCategos(){
    var id_cat=$('#id_catego option:selected').val();
    if($("#tipo2").is(":checked")==true){ //combo
        var tip_pp = 1;
    }else if($("#tipo1").is(":checked")==true){
       var tip_pp= 0;
    }
    var repetido=0;
    
    var TABLA   = $("#table-cats tbody > tr");
    TABLA.each(function(){         
        var ncat   = $(this).find("input[id*='id_categoria']").val();
        if (ncat==id_cat) {
            repetido=1;
        }
        con_cat++;
    });
    if(id_cat!=""){
        if (repetido==0 && tip_pp==1 || tip_pp==0 && con_cat==0) {
            var html ='<tr class="pro_catego_'+rowcats+'">\
                        <td>Cantidad de categorías</td>\
                        <td>\
                          <input type="hidden" class="form-control" id="tipo" value="'+tip_pp+'">\
                          <input type="hidden" class="form-control" id="id_cat_prod" value="0">\
                          <input type="hidden" class="form-control" id="id_categoria" value="'+$("#id_catego option:selected").val()+'">\
                          <input type="number" class="form-control" min="1" id="cantidad" value="1" style="max-width: 100px">\
                        </td>\
                        <td>'+$("#id_catego option:selected").data("name")+'</td>\
                        <td>\
                          <button class="btn btn-raised gradient-flickr white sidebar-shadow"\
                            onclick="deletecatego('+rowcats+',0)" title="Eliminar"\
                            data-toggle="tooltip" data-placement="top"\
                            data-original-title="Eliminar">\
                                  <i class="fa fa-times"></i>\
                                </button>\
                        </td>\
                      </tr>';
            $('.btable-cats').append(html);
            con_cat++;
            rowcats++;
        }else if(repetido>0 && tip_pp==1){
            toastr.error('Error!', 'Asignada previamente');
        }
        else if(tip_pp==0 && con_cat!=0){
            toastr.error('Error!', 'Solo se puede asignar una categoría');
        }
    }else{
       toastr.error('Error!', 'Elige una categoría valida'); 
    }
    //console.log("con_cat: "+con_cat);
    //console.log("tip_: "+tip_pp);
}
function deletecatego(row,id){
    if (row==0) {
        $('.pro_catego_'+row).remove();
    }else{
        //===========
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar esta categoría? El cambio será irreversible',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                // Si le da click al botón de "confirmar"
                confirmar: function () 
                {
                    var uri = base_url+'Productos/deletecategos';
                    $.ajax({
                        type:'POST',
                        url: uri,
                        data: {id:row},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            toastr.success('Hecho!', 'Eliminado Correctamente');
                            $('.pro_si_catego_'+row).remove();
                            
                        }
                    });
                            
                },
                // Si le da click a "cancelar"
                cancelar: function () 
                {
                    
                }
            }
        });
        //===========
    }   
}

var rowinsumo=0;
function addinsumos(id,cant,insumoId,insumo,tipo){
    if (insumoId>0) {
        var optioninsumo='<option value="'+insumoId+'">'+insumo+'</option>';
    }else{
        var optioninsumo='';
    }
    var name_tp = ""; var name_cn = "";
    if(tipo==0){
        name_tp="Insumo";
        name_cn="(En gramos/ ml /pza(s))";
    }
    else{
        name_tp="Producto";
        name_cn="(En pza(s))";
    }

    var html ='<tr class="pro_insumo_'+rowinsumo+'">\
                    <td>Cantidad</td>\
                    <td>\
                      <input type="hidden" class="form-control" id="insumoId" value="'+id+'">\
                      <input type="number" class="form-control" id="cantidad" value="'+cant+'" style="max-width: 100px">\
                    </td>\
                    <td>'+name_cn+'</td>\
                    <td>'+name_tp+'</td>\
                    <td>\
                        <div style="width:200px">\
                          <select class="form-control insumo" id="insumo">\
                          '+optioninsumo+'\
                          </select>\
                        </div>\
                    </td>\
                    <td>\
                      <button class="btn btn-raised gradient-flickr white sidebar-shadow"\
                        onclick="deleteinsumo('+rowinsumo+','+id+','+tipo+')" title=""\
                        data-toggle="tooltip" data-placement="top"\
                        data-original-title="Eliminar">\
                              <i class="fa fa-times"></i>\
                            </button>\
                    </td>\
                  </tr>';
    $('.btable-insumos').append(html);
    rowinsumo++;
    select2insumos(tipo);
}
function deleteinsumo(row,id,tipo){
    if (id==0) {
        $('.pro_insumo_'+row).remove();
    }else{
        //===========
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar este Insumo? El cambio será irreversible',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                // Si le da click al botón de "confirmar"
                confirmar: function () 
                {
                    if(tipo==0){
                        uri = base_url+'Productos/deleteinsumos';
                    }else{
                        uri = base_url+'Productos/deleteprods';
                    }
                    $.ajax({
                        type:'POST',
                        url: uri,
                        data: {id:id},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            toastr.success('Hecho!', 'eliminado Correctamente');
                            $('.pro_insumo_'+row).remove();
                            
                        }
                    });
                            
                },
                // Si le da click a "cancelar"
                cancelar: function () 
                {
                    
                }
            }
        });
        //===========
    }
    
}
function select2insumos(tipo){
    if(tipo==0){
        $('.insumo').select2({
            width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un Insumo',
            ajax: {
                url: base_url+'Productos/searchinsumo',
                dataType: "json",
                data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data){
                var clientes=data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.insumosId,
                        text: element.insumo
                    });
                });
                return {
                    results: itemscli
                };          
            },  
          }
        });
    }else{
        $('.insumo').select2({
            width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un Producto',
            ajax: {
                url: base_url+'Productos/searchProducto',
                dataType: "json",
                data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data2){
                var itemscli = [];
                data2.forEach(function(element2) {
                    itemscli.push({
                        id: element2.productoid,
                        text: element2.codigo +"- "+element2.nombre
                    });
                });
                return {
                    results: itemscli
                };          
            },  
          }
        }); 
    }
}
function datosproducto(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Productos/datospro',
        data: {pro:id, tipo:tipo},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
            array.forEach(function(element) {
                if(tipo==0)
                    addinsumos(element.insumoId,element.cantidad,element.insumosId,element.insumo,tipo);
                else
                    addinsumos(element.id,element.cantidad,element.id_producto_combo,element.prod_name,tipo);
            });
        }
    });
}