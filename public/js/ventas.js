var base_url = $('#base_url').val();
var direccion = "";
var pagaComanda = 0;

$(document).ready(function () {

	$("#agregar_comanda").click(function (e) {
		pagaComanda = 1;
	});

	$('#vcliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
		ajax: {
			url: 'Ventas/searchcli',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var clientes = data;
				var itemscli = [];
				data.forEach(function (element) {
					itemscli.push({
						id: element.ClientesId,
						text: element.Nom,
						dir: "Calle: " + element.Calle + " " + element.noExterior + " # int. " + element.noInterior + ", Colonia " + element.Colonia + ", " + element.Municipio + " " + element.Estado
					});
				});
				return {
					results: itemscli
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
		direccion = data.dir;
		//console.log("direccion: "+direccion);
		$("#direcc").val(direccion);
		if ($("#tipo_costo option:selected").val() == 3) {
			$("#cont_direcc").show();
			//$("#direcc").val(direccion);
		} else {
			//$("#direcc").val('');
			$("#cont_direcc").hide();
		}

	});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
		ajax: {
			url: 'Ventas/searchproducto',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var clientes = data;
				var itemscli = [];
				data.forEach(function (element) {
					itemscli.push({
						id: element.productoid,
						text: element.codigo + ' / ' + element.nombre,
						tipo: element.tipo
					});
				});
				return {
					results: itemscli
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		addproducto(data.tipo);
	});
	$('#vproducto').select2('open').on('focus');
	$('#ingresaventa').click(function (event) {
		addventas();
	});
	$('#generarTicket').click(function (event) {
		addtickets();
	});
	$('#btnabrirt').click(function () {
		$.ajax({
			type: 'POST',
			url: 'Ventas/abrirturno',
			data: {
				cantidad: $('#cantidadt').val(),
				nombre: $('#nombredelturno').val()
			},
			async: false,
			success: function (data) {
				toastr.success('Turno Abierto', 'Hecho!');
				$('#modalturno').modal('hide');

			}
		});

	});
	$("#vingreso").keypress(function (e) {
		if (e.which == 13) {
			addventas();
		}
	});
	$("#vingresot").keypress(function (e) {
		if (e.which == 13) {
			addventas();
		}
	});

	$("#tipo_costo").on("change", function () {
		if ($("#tipo_costo").val() == 2) {
			$("#mpago").val(1);
			$("#mdescuento").attr("disabled", true);
			$("#vingresot").attr("disabled", true);
			$("#cont_mesa").show();
			$("#ingresaventa").html('Guardar Comanda');
			$("#agregar_comanda").show();

			$("#generarTicket").show();
		} else {
			$("#cont_mesa").hide();
			$("#ingresaventa").html('Ingresar venta');
			$("#agregar_comanda").hide();

			$("#generarTicket").hide();

			//Limpiar form - tabla

			limpiar();
			$("#vcliente").val(45).change();
			$('#vtotal').val('');
			$('#vcambio').val(0);
			$('#vingreso').val('');
			$('#vingresot').val('');
			$("#tipo_costo").attr("disabled", false);
			$('#tipo_costo').val(1);
			$("#mpago").attr("disabled", false);
			$("#mdescuento").attr("disabled", false);
			$("#vingreso").attr("disabled", false);
			$("#vingresot").attr("disabled", false);

			$('#mesa').val('');
			$('#cont_mesa').hide();
			$('#cont_direcc').hide();
			$("#ingresaventa").attr("disabled", false);
			$("#direcc").val('');
			$('#agregar_comanda').hide();
			selectComandas();

		}
		if ($("#tipo_costo").val() == 3) {
			$("#cont_direcc").show();
		} else {
			//$("#direcc").val('');
			$("#cont_direcc").hide();
		}
	});

	$("#vingreso").on("change", function () {
		calculartotal();
	});
	$("#vingresot").on("change", function () {
		calculartotal();
	});

	$("#venta_mesa").on("change", function () {
		id_venta = $("#venta_mesa option:selected").val();
		$("#id_venta").val(id_venta);
		if (id_venta > 0) {
			//$("#ingresaventa").html("Pagar comanda");
			buscarComCombo();
			$.ajax({
				type: 'POST',
				url: base_url + 'Ventas/buscarComanda',
				data: {
					id_venta: id_venta
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra la venta');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					//console.log(data);
					$("#tipo_costo").val(2);
					$('#class_productos').html(data);
					$("#agregar_comanda").show();
					$("#cont_mesa").show();
					$("#mesa").val($("#venta_mesa option:selected").data("mesac"));
					$("#ingresaventa").html('Guardar Comanda');
					calculartotal();

					$("#generarTicket").show();
				}
			});
		}
	});
	$('#agregar_comanda').click(function (event) {
		addventas();
	});
});

function buscarComCombo() {
	$.ajax({
		type: 'POST',
		url: base_url + 'Ventas/buscarComandaCombo',
		data: {
			id_venta: id_venta
		},
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error('Error!', 'No Se encuentra la venta');
			},
			500: function () {
				toastr.error('Error', '500');
			}
		},
		success: function (data) {
			$('#class_productos_combo').html(data);
			//---
			calculartotal();
		}
	});
}

function cambioProducto(aux, cont, id_subp) {
	//console.log("id_subp: "+id_subp);
	var nvo_cod = $(".prod_sel" + aux + "_" + cont + " option:selected").data("cod");
	$("#cod_prod" + aux + "_" + cont).html(nvo_cod);
	var cant_tot = $("#cant_prod" + aux + "_" + cont).val();
	var cant_cuentap = 0;
	var band_cta = 0;
	var cont_tabla = 0;
	var TABLAp = $("#productos_combo tbody > .de_producto_" + cont);
	TABLAp.each(function () {
		cont_tabla++;
		if ($(this).find("select[class*='prod_sel" + cont_tabla + "_" + cont + "'] option:selected").val() != "") { //!= vacio y misma clase aux y cont
			cant_cuentap++;
		}
		if (cant_cuentap == cant_tot) {
			band_cta = 1;
		}
	});
	//console.log("cant_cuentap: "+cant_cuentap);
	//console.log("band_cta: "+band_cta); 

	var cant_cuenta = 0;
	var cont_tabla2 = 0;
	var TABLA = $("#productos_combo tbody > .de_producto_" + cont);
	TABLA.each(function () {
		cont_tabla2++;
		if ($(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "'] option:selected").val() != "") {
			cant_cuenta++;
		}
		if (cant_cuenta == cant_tot && $(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "'] option:selected").val() == "" || band_cta == 1 && $(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "'] option:selected").val() == "") {
			$(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "']").attr("disabled", true);
		} else if (cant_cuenta < cant_tot && $(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "'] option:selected").val() == "") {
			$(this).find("select[class*='prod_sel" + cont_tabla2 + "_" + cont + "']").attr("disabled", false);
		}
		//console.log("nombre_prod: "+$(this).find("select[id*='nombre_prod'] option:selected").val());    
	});

	/*$.ajax({
	    type:'POST',
	    url: base_url+'Ventas/prodComboSelect',
	    data: {
	        id_producto: id_subp
	    },
	    success:function(data){
	    	//console.log(data);
	    }
	}); */
	//console.log("cant_cuenta: "+cant_cuenta);   
}
var count_combo = 0;


function addproducto(tipo) {
	//console.log("tipo: " + tipo);
	if (tipo == 0) {
		if ($('#vcantidad').val() > 0) {
			$.ajax({
				type: 'POST',
				url: base_url + 'Ventas/addproducto',
				data: {
					cant: $('#vcantidad').val(),
					prod: $('#vproducto').val(),
					tipo: $('#tipo_costo').val()
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					//console.log(data);
					if ($("#id_venta").val() == 0) {
						$('#class_productos').html(data);
					} else {
						$('#class_productos').children('[class*="producto_"]').not('[class*="row_"]').remove();
						
						$('#class_productos').append(data);
					}
				}
			});
			$('#vcantidad').val(1);
			$('#vproducto').html('');
			$("#vproducto").val(0).change();
			$('#vproducto').select2('open').on('focus');
		}
	} else {
		if ($('#vcantidad').val() > 0) {
			$.ajax({
				type: 'POST',
				url: 'Ventas/addproductoCombo',
				data: {
					cant: $('#vcantidad').val(),
					prod: $('#vproducto').val(),
					tipo: $('#tipo_costo').val(),
					count_combo: count_combo
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					//console.log(data);
					$('#productos_combo').append(data);
					count_combo++;
				}
			});
			$('#vcantidad').val(1);
			$('#vproducto').html('');
			$("#vproducto").val(0).change();
			$('#vproducto').select2('open').on('focus');
		}
	}
	calculartotal();
}

function calculartotal() {
	var addtp = 0;
	$(".vstotal").each(function () {
		var vstotal = $(this).val();
		addtp += Number(vstotal);
	});
	//var descuento1=100/$('#mdescuento').val();
	$('#vsbtotal').val(addtp);
	var descuento = addtp * $('#mdescuento').val();
	$('#cantdescuento').val(descuento);
	var total = parseFloat(addtp) - parseFloat(descuento);
	$('#vtotal').val(Number(total).toFixed(2));
	ingreso();
}

function ingreso() {
	var ingresoe = $('#vingreso').val();
	var ingresot = $('#vingresot').val();
	if (ingresoe == "") {
		ingresoe = 0;
	}
	if (ingresot == "") {
		ingresot = 0;
	}
	var ingreso = parseFloat(ingresoe) + parseFloat(ingresot);
	var totals = $('#vtotal').val();

	if (ingreso > 0)
		var cambio = parseFloat(ingreso) - parseFloat(totals);
	//if (cambio<0) {
	//	cambio=0;
	//}
	//console.log("ingreso: "+ingreso);
	//console.log("cambio: "+cambio);
	$('#vcambio').val(cambio);
}

function limpiar() {
	$('#class_productos').html('');
	$.ajax({
		type: 'POST',
		url: 'Ventas/productoclear',
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error('Error!', 'No Se encuentra el archivo');
			},
			500: function () {
				toastr.error('Error', '500');
			}
		},
		success: function (data) {

		}
	});
}

function deletepro(id, tipo) {

	$.ajax({
		type: 'POST',
		url: 'Ventas/deleteproducto',
		data: {
			idd: id
		},
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error('Error!', 'No Se encuentra el archivo');
			},
			500: function () {
				toastr.error('Error', '500');
			}
		},
		success: function (data) {
			if (tipo == 1) {
				$('.producto_combo_' + id).remove();
				$('.de_producto_' + id).remove();
			}else{
				$('.producto_' + id).remove();
			}
			calculartotal();
		}
	});
}

function addventas() {
	//if (/*$('#vcambio').val()>=0 || */parseFloat($("#vingreso").val())!=0 || parseFloat($("#vingresot").val()!=0)) {
	var TABLACT = $("#productos_combo tbody > #producto_tabla");
	var con_ele = 0;
	var cont_tot_prod = 0;
	var cont_prod = 0;
	var band_mesa = 1;
	TABLACT.each(function () {
		cont_prod = parseInt($(this).find("input[id*='cant_prod']").val());
		cont_tot_prod = cont_tot_prod + cont_prod;
	});
	var TABLACT2 = $("#productos_combo tbody > #de_producto");
	var con_ele = 0;
	TABLACT2.each(function () {
		if ($(this).find("select[id*='nombre_prod'] option:selected").val() != "") { //!= vacio y misma clase aux y cont
			con_ele++;
		}
	});
	if (con_ele == cont_tot_prod && $("#id_venta").val() == 0 || $("#id_venta").val() > 0) {
		if ($("#tipo_costo option:selected").val() == 2 && $("#mesa").val() == 0 || $("#tipo_costo option:selected").val() == 2 && $("#mesa").val() == "") {
			toastr.error('Ingresar número de mesa', 'Error!');
			band_mesa = 0;
		}
		if (parseFloat($('#vcambio').val()) >= 0 && $("#tipo_costo option:selected").val() == 1 || $("#tipo_costo option:selected").val() == 3 || $("#tipo_costo option:selected").val() == 2 && band_mesa == 1) { //genera la venta 
			/*console.log("vcambio: "+$('#vcambio').val());
			console.log("vingreso: "+$('#vingreso').val());
			console.log("vingresot: "+$('#vingresot').val());
			console.log("tipo_costo: "+$('#tipo_costo').val());*/

			var vcambio = $('#vcambio').val();
			var efec = $('#vingreso').val();
			var tar = $('#vingresot').val();
			if (efec == "") {
				efec = 0;
			}
			if (tar == "") {
				tar = 0;
			}
			var vpagacon = parseFloat(efec) + parseFloat(tar);
			var vingresot = $('#vingresot').val() == '' ? 0 : $('#vingresot').val();
			var vtotal = $('#vtotal').val();
			var metodo = $('#mpago').val();
			//var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
			var efectivo = $('#vingreso').val();
			if ($('#mpago').val() == 5) {
				//vingresot = parseFloat(vtotal);
			}
			var pagado = 1;
			if ($('#tipo_costo option:selected').val() == 2 && pagaComanda == 0) {
				pagado = 0;
			} else if ($('#tipo_costo option:selected').val() == 2 && pagaComanda == 1) {
				pagado = 1;
			} else if ($('#tipo_costo option:selected').val() == 3) {
				pagado = 0;
			}

			numVentaMesa = getNumMesa();
			//console.log("numMesas: " + numVentaMesa);

			if ($("#id_venta").val() == 0 && numVentaMesa == 0) {
				//console.log("IF");
				$.ajax({
					type: 'POST',
					url: 'Ventas/ingresarventa',
					data: {
						uss: $('#ssessius').val(),
						cli: $('#vcliente').val(),
						tipo_costo: $('#tipo_costo').val(),
						pagado: pagado,
						mesa: $('#mesa').val(),
						mpago: $('#mpago').val(),
						desc: $('#mdescuento').val(),
						descu: $('#cantdescuento').val(),
						sbtotal: $('#vsbtotal').val(),
						total: vtotal,
						efectivo: efectivo,
						tarjeta: vingresot
					},
					async: false,
					statusCode: {
						404: function (data) {
							toastr.error('Error!', 'No Se encuentra el archivo');
						},
						500: function () {
							toastr.error('Error', '500');
						}
					},
					beforeSend: function () {
						$("#ingresaventa").attr("disabled", true);
					},
					success: function (data) {
						var idventa = data;

						if ($("#productos_combo tbody > tr").length > 0) {
							var DATA2 = [];
							var TABLA2 = $("#productos_combo tbody > tr"); //falta guardar los combos 
							TABLA2.each(function () {
								item2 = {};
								item2["idventa"] = idventa;
								item2["producto"] = $(this).find("input[id*='vsproid']").val();
								item2["cantidad"] = $(this).find("input[id*='vscanti']").val();
								item2["precio"] = $(this).find("input[id*='vsprecio']").val();
								item2["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
								item2["de_prod"] = $(this).find("input[id*='de_prod']").val();
								item2["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
								if ($(this).find("input[id*='de_prod']").val() == 1) {
									item2["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
								} else {
									item2["id_prod_combo"] = 0;
								}
								DATA2.push(item2);
							});
							INFO2 = new FormData();
							aInfo2 = JSON.stringify(DATA2);
							INFO2.append('data', aInfo2);

							$.ajax({
								data: INFO2,
								type: 'POST',
								url: 'Ventas/ingresarventaproCombo',
								processData: false,
								contentType: false,
								async: false,
								statusCode: {
									404: function (data) {
										toastr.error('Error!', 'No Se encuentra el archivo');
									},
									500: function () {
										toastr.error('Error', '500');
									}
								},
								success: function (data) {
									//$('#productos_combo').html('');
									$('#class_productos_combo').html('');
								}
							});
						}

						var DATA = [];
						var TABLA = $("#productosv tbody > tr");
						TABLA.each(function () {
							item = {};
							item["idventa"] = idventa;
							item["producto"] = $(this).find("input[id*='vsproid']").val();
							item["cantidad"] = $(this).find("input[id*='vscanti']").val();
							item["precio"] = $(this).find("input[id*='vsprecio']").val();
							item["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
							item["de_prod"] = $(this).find("input[id*='de_prod']").val();
							item["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
							if ($(this).find("input[id*='de_prod']").val() == 1) {
								item["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
							} else {
								item["id_prod_combo"] = 0;
							}
							DATA.push(item);
						});
						INFO = new FormData();
						aInfo = JSON.stringify(DATA);
						INFO.append('data', aInfo);
						$.ajax({
							data: INFO,
							type: 'POST',
							url: 'Ventas/ingresarventapro',
							processData: false,
							contentType: false,
							async: false,
							statusCode: {
								404: function (data) {
									toastr.error('Error!', 'No Se encuentra el archivo');
								},
								500: function () {
									toastr.error('Error', '500');
								}
							},
							success: function (data) {

							}
						});
						checkprint = document.getElementById("checkimprimir").checked;
						if (checkprint == true && $("#tipo_costo option:selected").val() != 2) {
							$("#iframeri").modal();
							//$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');	
							$('#iframereporte').html('<iframe src="' + base_url + 'Ticket?id=' + idventa + '&vcambio=' + vcambio + '&vpagacon=' + vpagacon + '&metodo=' + metodo + '"></iframe>');
						} else {
							if ($("#tipo_costo option:selected").val() != 2) {
								toastr.success('Venta Realizada', 'Hecho!');
							} else {
								toastr.success('Comanda guardada con éxito', 'Hecho!');
							}
						}
						limpiar();
						$("#vcliente").val(45).change();
						$('#vtotal').val('');
						$('#vcambio').val(0);
						$('#vingreso').val('');
						$('#vingresot').val('');
						$("#tipo_costo").attr("disabled", false);
						$('#tipo_costo').val(1);
						$("#mpago").attr("disabled", false);
						$("#mdescuento").attr("disabled", false);
						$("#vingreso").attr("disabled", false);
						$("#vingresot").attr("disabled", false);

						$('#mesa').val('');
						$('#cont_mesa').hide();
						$('#cont_direcc').hide();
						$("#ingresaventa").attr("disabled", false);
						$("#direcc").val('');
						$('#agregar_comanda').hide();
						selectComandas();
					}
				});
			} //if de venta nueva
			else 
			{ //else de venta de comanda a editar
				//console.log("ELSE");
				if (parseFloat($('#vcambio').val()) >= 0 && pagaComanda == 1 || pagaComanda == 0) {
					//console.log("LOG: " + numVentaMesa);
					//console.log("LOG: " + (numVentaMesa>0 ? numVentaMesa : $("#id_venta").val()));

					idVenta = numVentaMesa > 0 ? numVentaMesa : $("#id_venta").val()

					//console.log("idVenta " + idVenta);

					$.ajax({
						type: 'POST',
						url: base_url + 'Ventas/editarVentaPago',
						data: {
							id_venta: idVenta,
							metodo: $('#mpago').val(),
							descuento: $('#mdescuento').val(),
							descuentocant: $('#cantdescuento').val(),
							subtotal: $('#vsbtotal').val(),
							monto_total: vtotal,
							efectivo: efectivo,
							pagotarjeta: vingresot,
							pagado: pagado

						},
						async: false,
						statusCode: {
							404: function (data) {
								toastr.error('Error!', 'No Se encuentra la venta');
							},
							500: function () {
								toastr.error('Error', '500');
							}
						},
						beforeSend: function () {
							$("#ingresaventa").attr("disabled", true);
							$("#agregar_comanda").attr("disabled", true);
						},
						success: function (data) {
							if ($("#productos_combo tbody > tr").length > 0) {
								var DATA2 = [];
								var TABLA2 = $("#productos_combo tbody > tr"); //falta guardar los combos 
								TABLA2.each(function () {
									item2 = {};
									item2["idventa"] = idVenta;
									item2["producto"] = $(this).find("input[id*='vsproid']").val();
									item2["cantidad"] = $(this).find("input[id*='vscanti']").val();
									item2["precio"] = $(this).find("input[id*='vsprecio']").val();
									item2["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
									item2["de_prod"] = $(this).find("input[id*='de_prod']").val();
									item2["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
									if ($(this).find("input[id*='de_prod']").val() == 1) {
										item2["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
									} else {
										item2["id_prod_combo"] = 0;
									}
									DATA2.push(item2);
								});
								INFO2 = new FormData();
								aInfo2 = JSON.stringify(DATA2);
								INFO2.append('data', aInfo2);

								$.ajax({
									data: INFO2,
									type: 'POST',
									url: 'Ventas/ingresarventaproCombo',
									processData: false,
									contentType: false,
									async: false,
									statusCode: {
										404: function (data) {
											toastr.error('Error!', 'No Se encuentra el archivo');
										},
										500: function () {
											toastr.error('Error', '500');
										}
									},
									success: function (data) {
										//$('#productos_combo').html('');
										$('#class_productos_combo').html('');
										//---
									}
								});
							}

							var DATA = [];
							var TABLA = $("#productosv tbody > tr");
							TABLA.each(function () {
								item = {};
								item["idventa"] = idVenta;
								item["producto"] = $(this).find("input[id*='vsproid']").val();
								item["cantidad"] = $(this).find("input[id*='vscanti']").val();
								item["precio"] = $(this).find("input[id*='vsprecio']").val();
								item["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
								item["de_prod"] = $(this).find("input[id*='de_prod']").val();
								item["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
								if ($(this).find("input[id*='de_prod']").val() == 1) {
									item["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
								} else {
									item["id_prod_combo"] = 0;
								}
								DATA.push(item);
							});
							INFO = new FormData();
							aInfo = JSON.stringify(DATA);
							INFO.append('data', aInfo);
							$.ajax({
								data: INFO,
								type: 'POST',
								url: 'Ventas/ingresarventapro',
								processData: false,
								contentType: false,
								async: false,
								statusCode: {
									404: function (data) {
										toastr.error('Error!', 'No Se encuentra el archivo');
									},
									500: function () {
										toastr.error('Error', '500');
									}
								},
								success: function (data) {

								}
							});

							checkprint = document.getElementById("checkimprimir").checked;
							if (checkprint == true && pagaComanda == 1) {
								$("#iframeri").modal();
								//$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');	
								idventa = $("#id_venta").val();
								$('#iframereporte').html('<iframe src="' + base_url + 'Ticket?id=' + idventa + '&vcambio=' + vcambio + '&vpagacon=' + vpagacon + '&metodo=' + metodo + '"></iframe>');
							} else {
								if (pagaComanda == 1) {
									toastr.success('Venta Realizada', 'Hecho!');
								} else {
									toastr.success('Comanda guardada con éxito', 'Hecho!');
								}
							}
							limpiar();
							$("#vcliente").val(45).change();
							$('#vtotal').val('');
							$('#vcambio').val(0);
							$('#vingreso').val('');
							$('#vingresot').val('');
							$("#tipo_costo").attr("disabled", false);
							$('#tipo_costo').val(1);
							$("#mpago").attr("disabled", false);
							$("#mdescuento").attr("disabled", false);
							$("#vingreso").attr("disabled", false);
							$("#vingresot").attr("disabled", false);

							$('#mesa').val('');
							$('#cont_mesa').hide();
							$('#cont_direcc').hide();
							$('#agregar_comanda').hide();
							$('#venta_mesa').val('0');
							$("#ingresaventa").html('Ingresar venta');
							pagaComanda = 0;
							$("#ingresaventa").attr("disabled", false);
							$("#agregar_comanda").attr("disabled", false);
							$("#direcc").val('');
							//$('#agregar_comanda').hide();
							selectComandas();
						}
					});
				} else {
					toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta', 'Error!');
				}
			} //else de venta a editar
		} else {
			toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta', 'Error!');
		}
	} else {
		toastr.error('Seleccione productos del combo a vender', 'Error!');
	}
}


function addtickets() {
	//console.log("Tickets");
	//if (/*$('#vcambio').val()>=0 || */parseFloat($("#vingreso").val())!=0 || parseFloat($("#vingresot").val()!=0)) {
	var TABLACT = $("#productos_combo tbody > #producto_tabla");
	var con_ele = 0;
	var cont_tot_prod = 0;
	var cont_prod = 0;
	var band_mesa = 1;

	TABLACT.each(function () {
		cont_prod = parseInt($(this).find("input[id*='cant_prod']").val());
		cont_tot_prod = cont_tot_prod + cont_prod;
	});

	var TABLACT2 = $("#productos_combo tbody > #de_producto");
	var con_ele = 0;

	TABLACT2.each(function () {
		if ($(this).find("select[id*='nombre_prod'] option:selected").val() != "") { //!= vacio y misma clase aux y cont
			con_ele++;
		}
	});

	if (con_ele == cont_tot_prod && $("#id_venta").val() == 0 || $("#id_venta").val() > 0) {
		if ($("#tipo_costo option:selected").val() == 2 && $("#mesa").val() == 0 || $("#tipo_costo option:selected").val() == 2 && $("#mesa").val() == "") {
			//toastr.error('Ingresar número de mesa', 'Error!');
			band_mesa = 0;
		}
		//if (parseFloat($('#vcambio').val()) >= 0 && $("#tipo_costo option:selected").val() == 1 || $("#tipo_costo option:selected").val() == 3 || $("#tipo_costo option:selected").val() == 2 && band_mesa == 1) { //genera la venta 
		if ($("#tipo_costo option:selected").val() == 2 && band_mesa == 1) { //genera la venta 
			/*console.log("vcambio: "+$('#vcambio').val());
			console.log("vingreso: "+$('#vingreso').val());
			console.log("vingresot: "+$('#vingresot').val());
			console.log("tipo_costo: "+$('#tipo_costo').val());*/

			var vcambio = $('#vcambio').val();
			var efec = $('#vingreso').val();
			var tar = $('#vingresot').val();
			if (efec == "") {
				efec = 0;
			}
			if (tar == "") {
				tar = 0;
			}
			var vpagacon = parseFloat(efec) + parseFloat(tar);
			var vingresot = $('#vingresot').val() == '' ? 0 : $('#vingresot').val();
			var vtotal = $('#vtotal').val();
			var metodo = $('#mpago').val();
			//var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
			var efectivo = $('#vingreso').val();
			if ($('#mpago').val() == 5) {
				//vingresot = parseFloat(vtotal);
			}
			var pagado = 1;
			if ($('#tipo_costo option:selected').val() == 2 && pagaComanda == 0) {
				pagado = 0;
			} else if ($('#tipo_costo option:selected').val() == 2 && pagaComanda == 1) {
				pagado = 1;
			} else if ($('#tipo_costo option:selected').val() == 3) {
				pagado = 0;
			}

			numVentaMesa = getNumMesa();
			//console.log("numMesas: " + numVentaMesa);

			if ($("#id_venta").val() == 0 && numVentaMesa == 0) {
				$.ajax({
					type: 'POST',
					url: 'Ventas/ingresarventa',
					data: {
						uss: $('#ssessius').val(),
						cli: $('#vcliente').val(),
						tipo_costo: $('#tipo_costo').val(),
						pagado: pagado,
						mesa: $('#mesa').val(),
						mpago: $('#mpago').val(),
						desc: $('#mdescuento').val(),
						descu: $('#cantdescuento').val(),
						sbtotal: $('#vsbtotal').val(),
						total: vtotal,
						efectivo: efectivo,
						tarjeta: vingresot
					},
					async: false,
					statusCode: {
						404: function (data) {
							toastr.error('Error!', 'No Se encuentra el archivo');
						},
						500: function () {
							toastr.error('Error', '500');
						}
					},
					beforeSend: function () {
						$("#ingresaventa").attr("disabled", true);
					},
					success: function (data) {
						var idventa = data;

						if ($("#productos_combo tbody > tr").length > 0) {
							var DATA2 = [];
							var TABLA2 = $("#productos_combo tbody > tr"); //falta guardar los combos 
							TABLA2.each(function () {
								item2 = {};
								item2["idventa"] = idventa;
								item2["producto"] = $(this).find("input[id*='vsproid']").val();
								item2["cantidad"] = $(this).find("input[id*='vscanti']").val();
								item2["precio"] = $(this).find("input[id*='vsprecio']").val();
								item2["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
								item2["de_prod"] = $(this).find("input[id*='de_prod']").val();
								item2["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
								if ($(this).find("input[id*='de_prod']").val() == 1) {
									item2["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
								} else {
									item2["id_prod_combo"] = 0;
								}
								DATA2.push(item2);
							});
							INFO2 = new FormData();
							aInfo2 = JSON.stringify(DATA2);
							INFO2.append('data', aInfo2);

							$.ajax({
								data: INFO2,
								type: 'POST',
								url: 'Ventas/ingresarventaproCombo',
								processData: false,
								contentType: false,
								async: false,
								statusCode: {
									404: function (data) {
										toastr.error('Error!', 'No Se encuentra el archivo');
									},
									500: function () {
										toastr.error('Error', '500');
									}
								},
								success: function (data) {
									//$('#productos_combo').html('');
									$('#class_productos_combo').html('');
								}
							});
						}

						var DATA = [];
						var TABLA = $("#productosv tbody > tr");
						TABLA.each(function () {
							item = {};
							item["idventa"] = idventa;
							item["producto"] = $(this).find("input[id*='vsproid']").val();
							item["cantidad"] = $(this).find("input[id*='vscanti']").val();
							item["precio"] = $(this).find("input[id*='vsprecio']").val();
							item["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
							item["de_prod"] = $(this).find("input[id*='de_prod']").val();
							item["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
							if ($(this).find("input[id*='de_prod']").val() == 1) {
								item["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
							} else {
								item["id_prod_combo"] = 0;
							}
							DATA.push(item);
						});
						INFO = new FormData();
						aInfo = JSON.stringify(DATA);
						INFO.append('data', aInfo);
						$.ajax({
							data: INFO,
							type: 'POST',
							url: 'Ventas/ingresarventapro',
							processData: false,
							contentType: false,
							async: false,
							statusCode: {
								404: function (data) {
									toastr.error('Error!', 'No Se encuentra el archivo');
								},
								500: function () {
									toastr.error('Error', '500');
								}
							},
							success: function (data) {

							}
						});

						//Comanda guardada con éxito 1

						$("#iframeri").modal();
						$('#iframereporte').html('<iframe src="' + base_url + 'Ticket_temp?id=' + idventa + '&vcambio=' + vcambio + '&vpagacon=' + vpagacon + '&metodo=' + metodo + '"></iframe>');

						limpiar();
						$("#vcliente").val(45).change();
						$('#vtotal').val('');
						$('#vcambio').val(0);
						$('#vingreso').val('');
						$('#vingresot').val('');
						$("#tipo_costo").attr("disabled", false);
						$('#tipo_costo').val(1);
						$("#mpago").attr("disabled", false);
						$("#mdescuento").attr("disabled", false);
						$("#vingreso").attr("disabled", false);
						$("#vingresot").attr("disabled", false);

						$('#mesa').val('');
						$('#cont_mesa').hide();
						$('#cont_direcc').hide();
						$("#ingresaventa").attr("disabled", false);
						$("#direcc").val('');
						$('#agregar_comanda').hide();
						selectComandas();

						$("#venta_mesa").val(idventa).change();
					}
				});
				
				

			} //if de venta nueva
			else 
			{ //else de venta de comanda a editar
				if (parseFloat($('#vcambio').val()) >= 0 && pagaComanda == 1 || pagaComanda == 0) {
					
					idVenta = numVentaMesa > 0 ? numVentaMesa : $("#id_venta").val()

					$.ajax({
						type: 'POST',
						url: base_url + 'Ventas/editarVentaPago',
						data: {
							id_venta: idVenta,
							metodo: $('#mpago').val(),
							descuento: $('#mdescuento').val(),
							descuentocant: $('#cantdescuento').val(),
							subtotal: $('#vsbtotal').val(),
							monto_total: vtotal,
							efectivo: efectivo,
							pagotarjeta: vingresot,
							pagado: pagado

						},
						async: false,
						statusCode: {
							404: function (data) {
								toastr.error('Error!', 'No Se encuentra la venta');
							},
							500: function () {
								toastr.error('Error', '500');
							}
						},
						beforeSend: function () {
							$("#ingresaventa").attr("disabled", true);
							$("#agregar_comanda").attr("disabled", true);
						},
						success: function (data) {
							if ($("#productos_combo tbody > tr").length > 0) {
								var DATA2 = [];
								var TABLA2 = $("#productos_combo tbody > tr"); //falta guardar los combos 
								TABLA2.each(function () {
									item2 = {};
									item2["idventa"] = idVenta;
									item2["producto"] = $(this).find("input[id*='vsproid']").val();
									item2["cantidad"] = $(this).find("input[id*='vscanti']").val();
									item2["precio"] = $(this).find("input[id*='vsprecio']").val();
									item2["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
									item2["de_prod"] = $(this).find("input[id*='de_prod']").val();
									item2["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
									if ($(this).find("input[id*='de_prod']").val() == 1) {
										item2["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
									} else {
										item2["id_prod_combo"] = 0;
									}
									DATA2.push(item2);
								});
								INFO2 = new FormData();
								aInfo2 = JSON.stringify(DATA2);
								INFO2.append('data', aInfo2);

								$.ajax({
									data: INFO2,
									type: 'POST',
									url: 'Ventas/ingresarventaproCombo',
									processData: false,
									contentType: false,
									async: false,
									statusCode: {
										404: function (data) {
											toastr.error('Error!', 'No Se encuentra el archivo');
										},
										500: function () {
											toastr.error('Error', '500');
										}
									},
									success: function (data) {
										//$('#productos_combo').html('');
										$('#class_productos_combo').html('');
									}
								});
							}

							var DATA = [];
							var TABLA = $("#productosv tbody > tr");
							TABLA.each(function () {
								item = {};
								item["idventa"] = idVenta;
								item["producto"] = $(this).find("input[id*='vsproid']").val();
								item["cantidad"] = $(this).find("input[id*='vscanti']").val();
								item["precio"] = $(this).find("input[id*='vsprecio']").val();
								item["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
								item["de_prod"] = $(this).find("input[id*='de_prod']").val();
								item["id_detalle_venta"] = $(this).find("input[id*='id_detalle_venta']").val();
								if ($(this).find("input[id*='de_prod']").val() == 1) {
									item["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
								} else {
									item["id_prod_combo"] = 0;
								}
								DATA.push(item);
							});
							INFO = new FormData();
							aInfo = JSON.stringify(DATA);
							INFO.append('data', aInfo);
							$.ajax({
								data: INFO,
								type: 'POST',
								url: 'Ventas/ingresarventapro',
								processData: false,
								contentType: false,
								async: false,
								statusCode: {
									404: function (data) {
										toastr.error('Error!', 'No Se encuentra el archivo');
									},
									500: function () {
										toastr.error('Error', '500');
									}
								},
								success: function (data) {

								}
							});

							
							$("#iframeri").modal();
							//idventa = $("#id_venta").val();
							$('#iframereporte').html('<iframe src="' + base_url + 'Ticket_temp?id=' + idVenta + '&vcambio=' + vcambio + '&vpagacon=' + vpagacon + '&metodo=' + metodo + '"></iframe>');
							
							
							limpiar();
							$("#vcliente").val(45).change();
							$('#vtotal').val('');
							$('#vcambio').val(0);
							$('#vingreso').val('');
							$('#vingresot').val('');
							$("#tipo_costo").attr("disabled", false);
							$('#tipo_costo').val(1);
							$("#mpago").attr("disabled", false);
							$("#mdescuento").attr("disabled", false);
							$("#vingreso").attr("disabled", false);
							$("#vingresot").attr("disabled", false);

							$('#mesa').val('');
							$('#cont_mesa').hide();
							$('#cont_direcc').hide();
							$('#agregar_comanda').hide();
							$('#venta_mesa').val('0');
							$("#ingresaventa").html('Ingresar venta');
							pagaComanda = 0;
							$("#ingresaventa").attr("disabled", false);
							$("#agregar_comanda").attr("disabled", false);
							$("#direcc").val('');
							//$('#agregar_comanda').hide();
							selectComandas();

							$("#venta_mesa").val(idVenta).change();
						}
					});
				} else {
					toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta', 'Error!');
				}
			} //else de venta a editar
		} else {
			toastr.error('Ingresar número de mesa', 'Error!');
		}
	} else {
		toastr.error('Seleccione productos del combo a vender', 'Error!');
	}
}



function selectComandas() {
	$.ajax({
		type: 'POST',
		url: base_url + 'Ventas/getselectComandas',
		async: false,
		success: function (data) {
			//console.log(data);
			$("#venta_mesa").html(data);
		}
	});
}

function getNumMesa(){
	mesa = 0;

	if($("#tipo_costo").val() == 2 && $("#mesa").val() > 0){
		$.ajax({
			type: 'POST',
			url: 'Ventas/consultarMesa',
			data: {
				numMesa: $("#mesa").val(),
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error('Error!', 'No Se encuentra el archivo');
				},
				500: function () {
					toastr.error('Error', '500');
				}
			},
			success: function (data) {
				//console.log("data: " + data);
				mesa = data;
			}
		});
	}else{
		mesa = 0;
	}
	
	//console.log("MESA: " + mesa);
	return mesa;

}


function deleteproComanda(id,count,tipo){
	$('#idProComan').val(id);
	$('#rowComanda').val(count);
	$('#rowTipo').val(tipo);
	$('#eliminacion').modal();
}


function deleteproductComanda() {
	var idPro = $('#idProComan').val();
	var row = $('#rowComanda').val();
	var tipoRow = $('#rowTipo').val();

	if(idPro > 0){
		$.ajax({
			type: 'POST',
			url: 'Ventas/deleteProComanda',
			data: {
				idPro: idPro
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error('Error!', 'No Se encuentra el archivo');
				},
				500: function () {
					toastr.error('Error', '500');
				}
			},
			success: function (data) {
				toastr.success('Hecho!', 'eliminado Correctamente');
				//$(btn).closest('tr').remove();
				//$(tempAuxBtn).closest('tr').remove();
				//tempAuxBtn = '';

				console.log(idPro + ',' + row + ',' +tipoRow );

				if (tipoRow == 1) {
					$('.row_combo_' + row).remove();
					$('.de_row_' + row).remove();
				}else{
					$('.row_' + row).remove();
				}
				calculartotal();

			}
		});
	}
}