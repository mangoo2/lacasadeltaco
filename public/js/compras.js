$(document).ready(function() {	
	$('#cproveedor').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un proveedor',
	  	ajax: {
	    	url: 'Compras/searchpro',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var itemspro = [];
	    	data.forEach(function(element) {
                itemspro.push({
                    id: element.id_proveedor,
                    text: element.razon_social
                });
            });
            return {
                results: itemspro
            };	    	
	    },  
	  }
	});
	buscProdIns();
	$("#chk_ins").on("click",function(){
		buscProdIns();
		if($("#chk_ins").is(":checked")){

		}else{
			$("#vcant_env").val('');
		}
	});

	$("#cant").on("change",function(){
		if($("#cant").val()!=""){
			var cant = $("#cant").val();
			var cantenv = $("#vcant_env").val();
			$("#vcantidad").val(cant*cantenv)
		}
	});
	$("#vcant_env").on("change",function(){
		if($("#cant").val()!=""){
			var cant = $("#cant").val();
			var cantenv = $("#vcant_env").val();
			$("#vcantidad").val(cant*cantenv)
		}
	});

	$('#ingresaventa').click(function(event) { 
        $.ajax({
            type:'POST',
            url: 'Compras/ingresarcompra',
            data: {
            	uss: $('#ssessius').val(),
                prov: $('#cproveedor').val(),
                desc: $('#mdescuento').val(),
                total: $('#vtotal').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	var idcompra=data;
            	var DATA  = [];
    			var TABLA   = $("#productosv tbody > tr");
				TABLA.each(function(){         
	                item = {};
	                item ["idcompra"] = idcompra;
	                item ["insumo"]   = $(this).find("input[id*='vsinsid']").val();
	                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
	                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
	                item ["cant_env"]  = $(this).find("input[id*='cant_env']").val();
	                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
	                DATA.push(item);
	            });
				INFO  = new FormData();
				aInfo   = JSON.stringify(DATA);
				INFO.append('data', aInfo);
	            $.ajax({
	                data: INFO,
	                type: 'POST',
	                url : 'Compras/ingresarcomprapro',
	                processData: false, 
	                contentType: false,
	                success: function(data){
	                }
	            });
	            toastr.success('Hecho!', 'Guardado Correctamente');
	            limpiar();
	            $('#vcantidad').val('')
				$('#vproducto').html('');
				$('#cprecio').val('');
				$('#cant').val('')
				$('#vcant_env').val('');
				$("#idinsumo").val('');
				$('#total_conceptos').val('');
			    $("#vproducto").val(0).change();
			    $('#vtotal').val(0);
            }
        });
	});

	$("#cprecio").on("change",function(){
		tot_concep = $("#cprecio").val()*$("#cant").val();
		$('#total_conceptos').val(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(tot_concep));
	});
	$("#cant").on("change",function(){
		tot_concep = $("#cprecio").val()*$("#cant").val();
		$('#total_conceptos').val(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(tot_concep));
	});

});

function buscProdIns(){
	if($("#chk_ins").is(":checked")){
		$("#name_search").html("Insumos");
		$("#vcant_env").attr("disabled",false);
		$('#vproducto').select2({
			width: 'resolve',
			minimumInputLength: 3,
			minimumResultsForSearch: 10,
			placeholder: 'Buscar un Insumo',
		  	ajax: {
		    	url: 'Compras/searchInsumo',
		    	dataType: "json",
		    	data: function (params) {
		      	var query = {
		        	search: params.term,
		        	type: 'public'
		      	}
		      	return query;
		    },
		    processResults: function(data){
		    	var clientes=data;
		    	var itemscli = [];
		    	data.forEach(function(element) {
	                itemscli.push({
	                    id: element.insumosId,
	                    text: element.nom_insumo,
	                    precio: element.preciocompra,
	                    insumoId: element.insumosId
	                });
	            });
	            return {
	                results: itemscli
	            };	    	
		    },  
		}
		}).on('select2:select', function (e) {
		    var data = e.params.data;
		    console.log(data.insumoId);
		    $('#cprecio').val(data.precio);
		    $("#idinsumo").val(data.insumoId);
		    tot_concep = data.precio*$("#cant").val();
		    if($("#cprecio").val()!="" && $("#cprecio").val()>0){
		    	$('#total_conceptos').val(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(tot_concep));
		    }
		});
	}else{
		$("#name_search").html("Productos");
		$("#vcant_env").attr("disabled",true);
		$('#vproducto').select2({
			width: 'resolve',
			minimumInputLength: 3,
			minimumResultsForSearch: 10,
			placeholder: 'Buscar un Producto',
		  	ajax: {
		    	url: 'Compras/searchProd',
		    	dataType: "json",
		    	data: function (params) {
		      	var query = {
		        	search: params.term,
		        	type: 'public'
		      	}
		      	return query;
		    },
		    processResults: function(data){
		    	var clientes=data;
		    	var itemscli = [];
		    	data.forEach(function(element) {
	                itemscli.push({
	                    id: element.productoid,
	                    text: element.codigo+"/"+element.nombre,
	                    preciocompra: element.preciocompra,
	                });
	            });
	            return {
	                results: itemscli
	            };	    	
		    },  
		}
		}).on('select2:select', function (e) {
		    var data = e.params.data;
		    //console.log("id: "+data.id);
		    $('#cprecio').val(data.preciocompra);
		    tot_concep = data.preciocompra*$("#cant").val();
		    if($("#cprecio").val()!="" && $("#cprecio").val()>0){
		    	$('#total_conceptos').val(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(tot_concep));
		    }
		});
	}
}

function addproducto(){
	//if ($('#vcantidad').val()>0) {
	if ($('#cant').val()>0) {
		if($("#chk_ins").is(":checked")){//insumo
			tipo=1;
		}else{
			tipo=0;
		}
		$.ajax({
	        type:'POST',
	        url: 'Compras/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val(),
	            prec: $('#cprecio').val(),
	            tipo: tipo
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                console.log(array);
                var selecttotal = Number((parseFloat($('#cant').val())*parseFloat(array.precio)).toFixed(2));
                if($("#chk_ins").is(":checked")){
	                var producto='<tr class="producto_'+array.id+'">\
	                <td><input type="hidden" name="vsproid" id="vsproid" value="'+array.id+'">'+array.codigo+'</td>\
	                <input type="hidden" name="vsinsid" id="vsinsid" value="'+$("#idinsumo").val()+'"></td>\
	                <td><input type="number" name="vscanti" id="vscanti" value="'+$('#cant').val()+'" readonly style="background: transparent;border: 0px;width: 80px;">\
	                <input type="hidden" name="cant_env" id="cant_env" value="'+array.cant+'" readonly style="background: transparent;border: 0px;width: 80px;"></td>\
	                <td>'+array.producto+'</td>\
	                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'+array.precio+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
	                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+selecttotal+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
	                <td><a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+array.id+')"><i class="ft-trash font-medium-3"></i></a></td></tr>';
	            }else{
	            	var producto='<tr class="producto_'+array.id+'">\
	                <td><input type="hidden" name="vsproid" id="vsproid" value="'+array.id+'">'+array.codigo+'</td>\
	                <input type="hidden" name="vsinsid" id="vsinsid" value="0"></td>\
	                <td><input type="number" name="vscanti" id="vscanti" value="'+$('#cant').val()+'" readonly style="background: transparent;border: 0px;width: 80px;">\
	                <input type="hidden" name="cant_env" id="cant_env" value="0"></td>\
	                <td>'+array.producto+'</td>\
	                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'+array.precio+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
	                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+selecttotal+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
	                <td><a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+array.id+')"><i class="ft-trash font-medium-3"></i></a></td></tr>';
	            }
                $('#class_productos').append(producto);
            }
        });
		$('#vcantidad').val('')
		$('#vproducto').html('');
		$('#cprecio').val('');
		$('#cant').val('')
		$('#vcant_env').val('');
		$("#idinsumo").val('');
		$('#total_conceptos').val('');
	    $("#vproducto").val(0).change();
	}
    calculartotal();
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    
    $('#vtotal').val(Number(addtp).toFixed(2));
}
function limpiar(){
	$('#class_productos').html('');
}
function deletepro(id){
	$('.producto_'+id).remove();
}